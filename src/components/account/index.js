import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Style from './style.module.css'
import { LockIcon, AccountCircleIcon, Divider } from "../icons/icons";
import { Link } from "react-router-dom";

const AccountManager = () =>{
    const data = useSelector((state)=>state)
    const [profile, setProfile] = useState({})

    useEffect(()=>{
        setProfile(data?.authSlice.user)
        console.log(data)
    }, [])
    return (
        <div className="container">
            <div className={Style.name}>
                <div className={Style.nameDiv}>
                    <span></span>
                    <h4 className={Style.name}>{profile.first_name} {profile.last_name}</h4>
                </div>
            </div>
            <div className={`row ${Style.row}`} >
                <div className={`col-sm-9 col-md-4 col-lg-4 ${Style.profileDiv}`}>
                        <div className={Style.profileHeader}>
                            <span >
                                <AccountCircleIcon className={Style.icon}/>
                            </span>
                            <p>Profile Detail</p>

                        </div>
                        <hr className={Style.hr}/>
                        <Link to={`/user/${profile.id}/profile-detail`} className={Style.link}><p className={Style.txt}>Basic Detail</p></Link>
                </div>
                <div className={`col-sm-0 col-md-5 col-lg-5 ${Style.securityDiv}`}>
                    <div className={Style.securityHeader}>
                        <span className={Style.icon}>
                            <LockIcon />
                        </span>
                        <p> Security Settings</p>

                    </div>
                    <hr className={Style.hr}/>
                    <Link className={Style.link} to={`/user/${profile.id}/change-password`}><p className={Style.txt}>Change Password</p></Link>
                    <Link className={Style.link}><p className={Style.txt}>Pin Setting</p></Link>
                    <Link className={Style.link}><p className={Style.del}> Delete Account</p></Link>
                </div>

            </div>
        </div>
    )

}
export default AccountManager