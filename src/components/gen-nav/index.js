import React, {useEffect, useState} from "react";
import { useSelector } from "react-redux";
import  Nav  from "react-bootstrap/Nav";
import Navbar from 'react-bootstrap/Navbar'
import  NavDropdown  from "react-bootstrap/NavDropdown";
import Container from 'react-bootstrap/Container'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';
import NavStyle from './genNav.module.css'
import { Badge } from "react-bootstrap";
import AuthService from "../../services/auth/auth-service";
import { WorkOutlineIcon, FavoriteBorderIcon, AccountCircleIcon, ShoppingCartIcon, LogoutIcon } from "../icons/icons";
import { useNavigate } from "react-router-dom";

const GenNavBar = ()=>{
  const [cart, setCart] = useState()
  const state = useSelector((state)=>state)
  const [user, setUser] = useState({})
  const [isLoggedIn, setIsLoggedIn] = useState(Boolean)   
  const nav = useNavigate()
  
  function GetCart(){
    let total
    useSelector((state)=>{
      total = state?.cartSlice.cart?.length

    })
    return total

  }
  
  function logout(){
    AuthService.logout()
    nav('/')
  }


  useEffect(()=>{
    setCart(state.cartSlice.cart.length)
    setUser(state.authSlice.user)
    setIsLoggedIn(AuthService.isLoggedIn)

  })

  return (
    <Navbar collapseOnSelect fixed="top" expand="lg" bg="dark" variant="dark">
    <Container>
      <Navbar.Brand href="/">Neat Store</Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
      <div className={`${NavStyle.searchField}`}>
        <Form className="d-flex">
          <Form.Control
            type="search"
            placeholder="Search"
            className={`me-10 `}
            aria-label="Search"
          />
          <Button className={`${NavStyle.btn}`} variant="outline-success">Search</Button>
        </Form>

        </div>
        <Nav className="me-auto">
        {
            isLoggedIn ? 
              <NavDropdown title= {user.first_name ? `Hi, ${user?.first_name}` : ' '} id="collasible-nav-dropdown">
                <NavDropdown.Item href="#action/3.1"></NavDropdown.Item>
                <NavDropdown.Item href={`/user/${user?.id}/account-detail`} className={NavStyle.item}>
                  <AccountCircleIcon className={NavStyle.icon}/>My Account
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="customer/orders">
                  <WorkOutlineIcon className={NavStyle.icon} />My Orders</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">
                  <FavoriteBorderIcon className={NavStyle.icon} /> My Saved Items
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item onClick={logout}>
                  <LogoutIcon className={NavStyle.icon} /> Logout
                </NavDropdown.Item>

              </NavDropdown>
            : 
            <NavDropdown title="Account" id="collasible-nav-dropdown">
              <NavDropdown.Item href="#action/3.1"></NavDropdown.Item>
              <NavDropdown.Item href="/login">
                Login
              </NavDropdown.Item>
              <NavDropdown.Divider />
              
              <NavDropdown.Item href="/signup">Register</NavDropdown.Item>
            </NavDropdown>
          }
          <NavDropdown title="Help" id="collasible-nav-dropdown">
            <NavDropdown.Item href="#action/3.1"></NavDropdown.Item>
            <NavDropdown.Item href="/login">
              Help Center
            </NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.4">
              Place & Track order
            </NavDropdown.Item>
            <NavDropdown.Divider />

            <NavDropdown.Item href="#action/3.4">
              Cancel order
            </NavDropdown.Item>
          </NavDropdown>
          <div className={`${NavStyle.cart}`}>
            <Nav.Link href="/shopping-cart">
              <ShoppingCartIcon/>
                <Badge>{cart}</Badge>
              </Nav.Link>
            
          </div>
        </Nav>
        
                  
      </Navbar.Collapse>
    </Container>
  </Navbar>

    )
}

export default GenNavBar