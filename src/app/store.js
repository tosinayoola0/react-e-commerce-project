import { configureStore, getDefaultMiddleware, combineReducers } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query'
import  storage  from 'redux-persist/lib/storage';
import {
  persistStore,
  persistReducer,
  FLUSH, 
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER
} from 'redux-persist'
import AuthReducer from '../slice/auth'
import thunk from "redux-thunk";
import DataReducer from '../slice/productSlice'
import MessageReducer from '../slice/msg'
import CategoryReducer from '../slice/categorySlice'
import ManufacturerReducer from '../slice/manufacturerSlice'
import ProfileReducer from '../slice/profileSlice'
import { CartReducer } from '../slice/cartSlice';
import { AddressSlice } from '../slice/addressSlice';

const persistConfig = {
  key: 'root',
  storage: storage,
  version: 1,
  //whitelist: [CartReducer]
}

const reducer = combineReducers({
  authSlice: AuthReducer,
  productSlice: DataReducer,
  messageSlice:MessageReducer,
  categorySlice: CategoryReducer,
  manufacturerSlice: ManufacturerReducer,
  cartSlice: CartReducer,
  addressSlice: AddressSlice,
  profileSlice: ProfileReducer
})

const persistedReducer = persistReducer(persistConfig, reducer)

export const store = configureStore({
  reducer: persistedReducer,
  devTools:true,
  //middleware: [thunk]
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
  
});

setupListeners(store.dispatch)
export default store