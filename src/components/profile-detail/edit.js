import React, { useState, useEffect } from "react";
import SettingsSidebar from "../sidebar/optional-siderbar";
import Profile from "../profile";


const EditProfile = ()=>{
    return (
        <div className="container">
            <div className="row">
                <div className="col-3">
                    <SettingsSidebar/>
                </div>
                <div className="col-8">
                    <Profile/>
                </div>
            </div>
        </div>
    )
}

export default EditProfile