import React, {useEffect, useState} from "react";
import CheckOutNav from "../gen-nav/checkout";
import Style from './style.module.css'
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { ArrowForwardIosIcon } from "../icons/icons";
import { config } from "../../environment/env";
import { useNavigate } from "react-router-dom";
import { Modal, Button } from "react-bootstrap";
import PickupModal from "../pickup-modal";

const ConfirmOrder = ()=>{
    const data = useSelector((data)=>data)
    const [items, setItems] = useState()
    const [deliveryCost, setDeliveryCost] = useState()
    const [showModal, setShowModal] = useState(false)
    const [address, setAddress] = useState() 
    const [user, setUser] = useState()
    const navigate = useNavigate()
    const initModal = () =>{
        setShowModal(!showModal)
    }


    useEffect(()=>{
        setAddress(data.addressSlice?.data)
        setItems(data?.cartSlice.cart)
        setUser(data)
    })

    const grandTotal = ()=>{
        let total = calculatePrice() + address?.address.price
        return total
    }

    function calculatePrice(){
        let total = 0
        items?.forEach((x)=>{
            total =+ x.total_price
        })
        return total
    }
    function confirmPurchase (){
        const data = {
            fullname: `${user?.authSlice.user.first_name} ${user?.authSlice.user.last_name}`,
            phone_number: user?.profileSlice?.profile.data?.phone_number,
            email: user?.authSlice.user?.email,
            pickup_address: `${address?.address?.address}  ${address?.lga}  ${address?.state}`,
            products: [items],
            total_cost: calculatePrice()
        }
        console.log(data)
        navigate('/confirm-order')

    }
    return(
        <div className={Style.body}>
            <CheckOutNav/>

            <div className={`container ${Style.container}`}>
                <div className="row">
                    <div className="col-lg-7 col-md-12 col-sm-12">
                        <div className={Style.address}>
                            <div className={Style.title}>
                                <h5 className={Style.h5}>1. CUSTOMER ADDRESS</h5>
                                <p >
                                    <Link className={Style.editLink}>Change <ArrowForwardIosIcon/></Link>
                                </p>
                            </div>                         
                            <hr/> 
                            <div>
                                <p className={Style.name}>
                                    {data?.authSlice.user.first_name} {data?.authSlice.user.last_name}
                                </p>
                                <p className={Style.addressInfo}>
                                    {data?.profileSlice.profile.data.address} | {data?.profileSlice.profile.data.state} | {data?.profileSlice.profile.data.phone_number}
                                </p>
                            </div>  
                        </div>
                        <div className={Style.address}>
                            <div className={Style.title}>
                                <h5 className={Style.h5}> 2. DELIVERRY DETAILS </h5>
                                

                            </div>
                            <hr/> 

                            <div className={Style.deliveryextra}>
                                <p>Pick-up Station</p>
                                <p className={Style.deliveryDate}>Delivery between </p>
                                <div className={Style.deliveyInfo}>
                                    <p className={Style.station}>
                                        <p className={Style.pickup}>Pickup Station</p>
                                        <p  className={Style.change}>
                                            <button className="btn btn-default" onClick={initModal}>  Change <ArrowForwardIosIcon/></button>
                                            <PickupModal showModal={showModal} setShowModal={setShowModal}/>
                                        </p>
                                    </p>
                                    <hr/>
                                    <div>
                                        <p className={Style.stateName}>{address?.address?.name}</p>
                                        <p className={Style.addressName}>{address?.address?.address} | {address?.lga} | {address?.state}</p> 
                                        
                                    </div>

                                </div>
                                <p className={Style.ship}>Shippment</p>
                                <div className={Style.shippment}> 
                                    <p>Pick-up Station</p>
                                    <p>Delivery between</p>
                                    <div>
                                        {items?.map((item)=>{
                                            return <div id={item.id} className={`row ${Style.row}`}>
                                                <div className={`col-2 ${Style.imgDiv}`}>
                                                    <img className={Style.img} src={`${config.imgUrl}/${item.img}`} />
                                                </div>
                                                <div className={`col-10 ${Style.item_names}`}>
                                                    <p>{item?.item_name}</p>
                                                    <p>QTY: {item?.quantity}</p>
                                                </div>
                                            </div>
                                        })}
                                    
                                    </div>  
                                </div>
                                <Link className={Style.cart} to={'/shopping-cart'}><p >MODIFY CART</p></Link>

                            </div>
                        </div>
                        <div className={Style.payment}>
                            <div className={Style.title}>
                                <h5 className={Style.h5}> 3. PAYMENT METHOD </h5>
                            </div>
                            <hr/> 
                            <div className={Style.payment}>
                                <p className={Style.paymentOption}>Pay with Cards</p>
                                <p>You will be redirect to our secure checkout page</p>
                            </div>
                        </div>
                        <Link to={'/'}>Go back & continue shopping </Link>
                    </div>
                    <div className={`col-lg-4 col-md-12 col-sm-12 ${Style.summary}`}>
                        <h5 className={Style.h5}>Order Summary</h5>
                        <hr/>
                        <p className={Style.price}><span className={Style.total}>Item's Total ({items?.length})</span>  <p>N{calculatePrice()}.00</p></p>
                        <p className={Style.price}><span className={Style.total}>Delivery Fees </span> <p>N{address?.address.price}.00</p></p>
                        <p className={Style.price}><span className={Style.total}>Total</span>  <p>N{grandTotal()}.00</p></p>
                        <button className={`btn btn-success ${Style.checkoutBtn}`} onClick={confirmPurchase}>Checkout</button>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default ConfirmOrder