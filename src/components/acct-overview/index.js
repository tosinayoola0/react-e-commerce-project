import React, {useState, useEffect} from "react";
import SideBar from "../sidebar/index";
import GenNavBar from "../gen-nav";
import Style from './acctStyle.module.css'
import Footer from "../footer";
import { useSelector } from "react-redux";
import { EditIcon, AccountBalanceWalletIcon, SecurityIcon, UserIcon } from "../icons/icons";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import {displayProfile} from '../../slice/profileSlice'


const AccountOverView = ()=>{
    const state = useSelector((state)=>state)
    const [profile, setProfile] = useState({})
    const [user, setUser] = useState({})
    const dispatch = useDispatch()

    useEffect(()=>{
        setUser(state.authSlice.user)
        getProfile()
    }, [])
   

    function getProfile(){
        dispatch(displayProfile(state.authSlice.user))
            .then((res)=>{
                setProfile(res?.payload.data)
            })
    }
   
    return (    
        <div className={Style.body}>
            <GenNavBar />
            <div className="container">
                <div className={Style.dummie}></div>
                <div className="row">
                    <div className={`${Style.SideBar} col-3`}>
                        <SideBar />
                    </div>
                    <div className={`${Style.content} col-8`}>
                        <h3>Account Overview</h3>
                        <div className={`row`}>
                            <div className={`col-5 ${Style.userInfo}` }>
                                <h5 className={Style.profileTitle}>ACCOUNT DETAILS</h5>
                                <div className={Style.profile}>
                                    <p className={Style.name}>{profile?.first_name} {profile?.last_name}</p>
                                    <p className={Style.email} >{user?.email}</p>
                                </div>  
                            </div>

                            <div className={`col-6 ${Style.addressInfo}` }>
                                <h5 className={Style.addressTitle}>
                                    ADRESS BOOK 
                                    <span >
                                        <Link to={`/user/${user.id}/profile`}>
                                            <EditIcon className={Style.icony}/>
                                        </Link>
                                    </span>
                                </h5>
                                <div className={Style.address}>
                                    <p className={Style.addressSubheading}>Your Default Address:</p>
                                    <p className={Style.name}>{profile?.first_name} {profile?.last_name}</p>
                                    <p className={Style.street} >{profile?.address}</p>
                                    <p className={Style.lga} >{profile?.lga}, {profile?.state}</p>
                                    <p className={Style.number} >{profile?.phone_number}</p>
                                </div>  
                            </div>
                        </div>
                        <div className="row">
                            <div className={`col-5 ${Style.walletInfo}`}>
                                <h5 className={Style.walletTitle}>NEAT STORE CREDIT </h5>
                                <div className={Style.walletContent}>
                                    <p className={Style.walletTxt}> 
                                        <span><AccountBalanceWalletIcon/></span>
                                        N0.00
                                    </p>
                                </div>
                            </div>
                            <div className={`col-6 ${Style.acctInfo}`}>
                                <h5 className={Style.acctTitle}>
                                    ACCOUNT MANAGER 
                                    <span>
                                        <Link to={`/user/${user.id}/profile-update`}><EditIcon className={Style.icon}/></Link>
                                    </span>
                                </h5>
                                <div className={Style.accountManager}>
                                    <p>
                                        <span>
                                            <UserIcon/>
                                        </span>
                                        Personal Detail
                                    </p>
                                    <p>
                                        <span>
                                            <SecurityIcon/>
                                        </span>
                                        Security Detail
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
        </div>
        
    )

}
export default AccountOverView