import React, {useState, useEffect} from 'react'
import * as Yup from 'yup'
import { useFormik, Formik, Form } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { useForm } from 'react-hook-form';
import { useNavigate } from "react-router-dom";
import { updateProfile, createProfile, displayProfile } from '../../slice/profileSlice';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Style from './profileStyle.module.css'
import GenNavBar from '../gen-nav';
import Footer from '../footer';
import 'react-phone-number-input/style.css'
import PhoneInput from 'react-phone-number-input'
import {yupResolver} from '@hookform/resolvers/yup'
const data = require('../../app/states.json')


const Profile = ()=>{
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const [user, setUser] = useState({})
    const [number, setNumber] = useState()
    const dataState = useSelector((state)=>state)
    const [states, setStates] = useState([])
    const [lgas, setLgas] = useState([])    
    const [key, setKey] = useState('')    
  
    const validationSchema = Yup.object().shape({
        first_name: Yup.string()
            .required('name is necesary'),
        last_name: Yup.string()
            .required('Surname is required'),
        phone_number : Yup.string()
            .required('phone number is required'),
        address: Yup.string()
            .required('Address is needed'),
        state: Yup.string()
            .required('state is required'),
        lga: Yup.string()
            .required('LGA is required')
    })
    
    function getStates(){
        let state = []
        data.forEach((x)=>{
            state.push(x.state)
            setStates(state)
        })
    }

    function getLgas (name){
        states.forEach((x)=>{
            if(name === x.name){
                setLgas(x.locals)
            }
        })        
    }

    
    const {register, handleSubmit, reset, setValue, getValues,   errors, formState} = useForm({
        resolver: yupResolver(validationSchema)
     }) 
     
    function createProfile(form){
        dispatch(createProfile(form))
            .then((res)=>{
                if(res.payload.status==200){
                    toast.success('profile created succesfully ', {autoClose: 2000, pauseOnHover:false})
                    navigate(`/user/${user.uuid}/addres-detail`)
                }{
                    toast.error('please try again', {autoClose:2000, pauseOnHover:false})
                }
            })
    }
    useEffect(()=>{
        setUser(dataState.authSlice.user)
        const newuser = dataState.authSlice.user
        if(user){
            dispatch(displayProfile(newuser))
                .then((res)=>{
                const fields = ['first_name', 'last_name', 'state', 'phone_number', 'address', 'lga']
                fields.forEach(field=> setValue(field, res?.payload.data[field]))
                setUser(res?.payload.data)
            })
        }
        getStates()
        getLgas(user.state)

    },[])

    function updateUserProfile(form){
        dispatch(updateProfile({form, user }))
            .then((res)=>{
                if(res.payload.status==200){
                    toast.success('profile updated succesfully ', {autoClose: 2000, pauseOnHover:false})
                    navigate(`/user/${user.uuid}/addres-detail`)
                    
                }
                else{
                    toast.error('please cross check your form and try again', {autoClose: 2000, pauseOnHover:false})
                }
            })
     }
    function onSubmit(data){
        if(user.first_name){
            updateUserProfile(data)
        }else{
            createProfile(data)
        }
    }
    
    
    return (
        <div className={Style.body}>
            <GenNavBar/>
            <ToastContainer/>
            <div className={`container ${Style.container}`}>
                <div className={`row ${Style.row}`}>
                    <div className={Style.dummy}></div>
                    <div className={`col-12 ${Style.formDiv}`}>
                        <h2 className={Style.title}>{user?.first_name ? 'Edit Profile' : 'Create Profile' }</h2>
                            <form onSubmit={handleSubmit(onSubmit)} onReset={reset}>
                                <div className={`row ${Style.row}`} >
                                    <div className={`form-group ${Style.fields} col-sm-12 col-md-6 col-lg-6` }>
                                        <input
                                            name='first_name'
                                            id='first_name'
                                            {... register('first_name',{
                                                required: "Required"
                                            })}
                                            placeholder='Enter Your first Name'
                                            className={`form-control`}
                                        />
                                    </div>

                                    <div className={`form-group ${Style.fields} col-sm-12 col-md-6 col-lg-6` }>
                                        <input
                                            name='last_name'
                                            id='last_name'
                                            placeholder='Enter Your Last Name'
                                            className={`form-control`}
                                            {... register('last_name',{
                                                required: "Required"
                                            })}
                                        />
                                    </div>
                                </div>
                                <div className={`row`}>
                                    <div className={`form-group ${Style.phoneDiv} col-sm-12 col-md-6 col-lg-6` }>           
                                        <PhoneInput
                                            {... register('phone_number',{
                                                required: "Required"
                                            })}
                                            name="phone_number"
                                            defaultCountry="NG"
                                            value={number}    
                                            onChange={setNumber}
                                            placeholder="enter your valid number"
                                            maxLength={15}
                                            className={ Style.number}
                                        />
                                    </div>
                                </div>
                                <div className={`row ${Style.row}`}>
                                    <div className={`form-group ${Style.fields} col-sm-12 col-md-12 col-lg-12` }>
                                        <textarea
                                            name='address'
                                            {... register('address',{
                                                required: "Required"
                                            })}
                                            placeholder='Enter Your Current Residetial Address'
                                            rows={5}
                                            cols={5}
                                            className={`form-control`}
                                        />
                                    </div>
                                </div>
                                <div className={`row ${Style.row}`}>
                                    <div className={`form-group ${Style.fields} col-sm-12 col-md-6 col-lg-6` }>
                                                
                                        <select 
                                        {... register('state',{
                                            required: "Required"
                                        })}
                                        onChange={ e=>getLgas(e.target.value)}

                                        >
                                            {states?.map((state)=>(
                                                <option 

                                                key={state.id}>
                                                    {state.name}
                                                </option>
                                            ))}
                                        </select>
                                    </div>
                                    <div className={`form-group ${Style.fields} col-sm-12 col-md-6 col-lg-6` }>
                                        <select
                                            {... register('lga',{
                                                required: "Required"
                                            })}
                                        >
                                            {lgas?.map(lg=>(
                                                <option key={lg.id}>
                                                    {lg.name}
                                                </option>
                                            ))}
                                        </select>

                                    </div>
                                </div>
                                <div className={`row ${Style.btnDiv}`}>
                                    <button type='submit' className={`btn btn-success ${Style.btn}`}>Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
         )

}

export default Profile