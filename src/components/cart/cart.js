import React, {useState, useEffect} from "react";
import { useSelector } from "react-redux";
import Style from './cartSyle.module.css'
import Divider from '@mui/material/Divider';
import { config } from "../../environment/env";
import GenNavBar from '../gen-nav';
import Footer from '../footer';
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { increaseQuantity, decreaseQuantity, calculateCost, deleteItem } from "../../slice/cartSlice";
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import ClearIcon from '@mui/icons-material/Clear';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AuthService from "../../services/auth/auth-service";
import { useNavigate } from "react-router-dom";


const Cart = ()=>{
    const state = useSelector((state)=>state)

    const [user, setUser] = useState({})
    const [carts , setCart] = useState([])
    const [itemPrice, setItemPrice] = useState('')
    const [totalPrice, setTotalPrice] = useState('')
    const [deliveryFee, setDeliveryFee] = useState('')
    const [quantity, setQuantity] = useState('')
    const dispatch = useDispatch()
    const nav =useNavigate()
    const [ isLoggedIn, setIsLoggedIn] = useState(Boolean)

    useEffect(()=>{
        setCart(state?.cartSlice.cart)
        setUser(state.authSlice.user)
        caltotalPrice(carts)
        setIsLoggedIn(AuthService.isLoggedIn())

    })
    
    function removeItem(item){
        dispatch(deleteItem(item))
        toast.success('item deleted successfuly', {position: toast.POSITION.TOP_RIGHT})

    }

    function decreaseItem(item){
        dispatch(decreaseQuantity(item))
        dispatch(calculateCost(item))

    }
    function increaseItem(item){
        dispatch(increaseQuantity(item))
        dispatch(calculateCost(item))
    }
    function caltotalPrice(items){
        let total = 0
        carts.forEach((x)=>{
            total += x.total_price
        })
        setItemPrice(total)
        setTotalPrice(total + deliveryFee)
    }

    function checkout(){
        if(isLoggedIn){
            if(!user.first_name){
                nav(`/user/${user.id}/profile`)   
            }else{
                nav('/confirm-order')
              console.log(isLoggedIn)    
            }
            
        }else{
            nav('/login')
            console.log('hi')
        }
    }

    return (
        <div >
            <GenNavBar/>
                <div className={Style.body}>
                <div></div>
                {
                    carts.length>0 ? 
                    <div className={`${Style.container} container`}>
                    <div className={`${Style.dummi} row`}>
                        <div className={`${Style.product} col-sm-12 col-md-12 col-lg-8`}>
                            <h3 >YOUR CART</h3>
                            <Divider/>
                            <div className={`${Style.tile} row`}>
                                <div className={`${Style.name} col-5`}>
                                    <h4 className={Style.title}>Item Details</h4>
                                </div>
                                <div className={`${Style.price} col-2`}>
                                    <h4 className={Style.title}>Unit Price</h4>
                                </div>
                                <div className={`${Style.quantity} col-2`}>
                                    <h4 className={Style.title}>Quantites</h4>
                                </div>
                                <div className={`${Style.name} col-2`}>
                                    <h4 className={Style.title}>Total Price</h4>
                                </div>
                                <div className={`${Style.name} col-2`}></div>

                            </div>
                            <Divider/>
                            <div className={Style.itemContainer}>
                                {carts.map((cart)=>{
                                    return <div className={`row ${Style.item}`} key={cart?.id}>
                                        <div className={`${Style.nameContent} col-5`}>
                                            <div className={`${Style.imgDiv}`}>
                                                <img src={`${config.imgUrl}/${cart.img}`} className={`${Style.img}`} alt="item image"/>
                                            </div>
                                            <div  className={`${Style.nameDiv}`}>
                                                <Link to={`/product/${cart?.id}/detail`} className={Style.link}>
                                                    <p className={Style.items}>{cart.item_name}</p>

                                                </Link>
                                            </div>
                                        </div>

                                        <div className={`${Style.priceContent} col-2`}>
                                            <div  className={`${Style.priceDiv}`}>
                                                <p className={Style.items} >N{cart.unit_price}</p>
                                            </div>
                                        </div>
                                        <div className={`${Style.quantityContent} col-2`}>
                                            <div  className={`${Style.quantityDiv}`}>
                                            <div className={Style.quantity}>
                                
                                                <span  onClick={()=>decreaseItem(cart)}><RemoveIcon className={Style.qtyIcon} /></span>
                                                <span className={Style.text}>{cart?.quantity}</span>
                                                <span onClick={()=>increaseItem(cart)} > <AddIcon className={Style.qtyIcon}/> </span>
                                                
                                
                                            </div>
                                            </div>
                                        </div>

                                        <div className={`${Style.quantityContent} col-2`}>
                                            <div  className={`${Style.quantityDiv}`}>
                                                <p className={Style.items}>N{cart.total_price}.00</p>
                                            </div>
                                        </div>
                                        <div className={`${Style.removeItem} col-1`} onClick={(()=>{removeItem(cart)})}>
                                            <ClearIcon/>
                                        </div>
                                    
                                        <Divider/>
                                    </div>
                                    
                                })}
                            </div>
                        </div>
                        <div className={`${Style.summaryContainer} col-sm-12 col-md-12 col-lg-3`}>
                            <div className={`${Style.summary}`}>
                                <h4>CART SUMMARY</h4> 
                            </div>
                            <Divider/>
                            <div className={Style.subTotal}>
                                <p className={Style.subText} >SubTotal</p>
                                <p className={Style.subPrice} > N{itemPrice}.00</p>

                            </div>
                            <Divider/>

                            <div className={Style.delivery}>
                                <p className={Style.deliverytxt} >Delivery charges</p>
                                <p className={Style.deliveryHnt} > {deliveryFee}</p>

                            </div>
                            <Divider/>
                            <div className={Style.totalDiv}>
                                <h3 className={Style.total}>Total: </h3 ><h3 className={Style.totalPrice}>N{totalPrice}.00</h3>

                            </div>
                            <Divider/>

                                <div className={Style.confirm}>
                                    <button className={`btn btn-success ${Style.btnConfirm}`} onClick={checkout}>Checkout</button>
                                </div>
                        </div>

                    </div>
                </div>
                : <div className={Style.empty}>
                     <div className={Style.cartIcon} >
                        <img src="/img/shopping-cart.png" className={Style.icon}/>
                     </div>
                     <h1 className={Style.emptyhead}>Your cart is empty</h1>   
                     <p className={Style.emptyTxt}>Browse our categories and discover our best deals!</p>
                    <button className={`${Style.btn} btn btn-success`}>Start Shopping</button>
                </div>
                }
                </div>
                
            <Footer/>
        </div>
    )
}

export default Cart