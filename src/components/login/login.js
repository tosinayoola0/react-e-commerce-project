import React from "react";
import { useFormik } from "formik";
import { Link } from "react-router-dom";
import * as Yup from 'yup'
import loginStyle from './login.module.css'
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { login } from "../../slice/auth";
import {addToCart} from '../../slice/cartSlice'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const validate = values =>{
    const errors = {}
    if(!values.email){
        errors.email = 'Email is required'
    }
    else if(!values.password){
        errors.password = 'Password is required'
    }
    return errors
}

const Login =() =>{
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const formik = useFormik({
        initialValues:{
            email: '',
            password: ''
        },
        validate,
        onSubmit :values =>{
            dispatch(login(values))
                .then((res)=>{
                    console.log(res?.payload?.success)
                    if(res?.payload?.success){
                        toast.success('login successful', {autoClose: 2000, pauseOnHover: false })
                        localStorage.setItem('user', JSON.stringify(res.payload))
                        console.log(res)
                        if(res?.payload.is_admin === true){
                            navigate(`/admin/${res.payload.id}/dashboard`)
                        
                    }
                    else{
                        navigate(`/`)
                    }

                    }
                },(err)=>{
                    console.log(err)})
                
        }
    })

    return (
        <div className={`${loginStyle.body}`}>
            <ToastContainer/>
            <div className={loginStyle.form}>
                <div className={`${loginStyle.content}`}>
                    <h3>Login</h3>
                    <hr/>
                    <button className={`${loginStyle.google} btn btn-primary` }>Login with google</button>

                    <div className={`${loginStyle.option} `}>
                        <hr className={`${loginStyle.hr}`}/>
                        <span className={`${loginStyle.or}`}> OR</span>
                        <hr className={`${loginStyle.hr}`}/>
                    </div>
                    <form onSubmit={formik.handleSubmit}>
                        <div className={`${loginStyle.fields} form-group`}>
                            <input
                                name="email"
                                placeholder="Enter your Email address"
                                value={formik.values.email}
                                onChange={formik.handleChange}
                                type="email"
                                className={`${loginStyle.input} form-control`}
                            />
                            {formik.errors.email ?(<span>{formik.errors.email}</span>): null}
                        </div>
                        <div className={`${loginStyle.fields} form-group`}>
                            <input
                                name="password"
                                placeholder="enter your password"
                                value={formik.values.password}
                                onChange={formik.handleChange}
                                type="password"
                                className={`${loginStyle.input} form-control`}
                            />
                            {formik.errors.password ? (<span> {formik.errors.password}</span>):null}
                        </div>
                        <p className={`${loginStyle.forget}`}>Forgot password?</p>
                        <button className={`btn btn-success ${loginStyle.btn}`}> login</button>
                    </form>
                    <p className={`${loginStyle.signup}`}>Don't have account,  <Link to={'/signup'}>Signup</Link></p>
                </div>
                
            </div>
        </div>
    )
}


export default Login