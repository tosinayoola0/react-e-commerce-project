import React from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import slider from "react-slick"
import Slider from "react-slick";
import SliderStyle from './style.module.css'

const ImageSlider = ({imgs}) =>{
    const settings = {
        infinite: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        lazyLoad: true,
        autoplay: true,
      autoplaySpeed: 2000,
       
      };
    return (
        <div className={`${SliderStyle.tag}`}>
            <div className={`imgSlider ${SliderStyle.slider}`}>
                <Slider {...settings}>
                    {imgs.map((item)=>(
                        <div key={item}>
                            <img src={item.src} alt={item.alt}/>
                        </div>
                    ))}
                </Slider>
            </div>
        </div>
    )

}

export default ImageSlider