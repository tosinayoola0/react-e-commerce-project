import { createSlice } from "@reduxjs/toolkit";

const initialState = {
}

const addressSlice = createSlice({
    name: 'address',
    initialState,
    reducers: {
        createAddress: (state, action)=>{
            return Object.assign({}, state, {
                data:action.payload
            })
        }
    }
})



export const  AddressSlice = addressSlice.reducer
export const {
    createAddress    
} = addressSlice.actions