import axios from "axios";
import { config } from "../../environment/env";
import  AuthInterceptor  from "../../interceptor/auth";

//const url = config.baseUrl

const userSignup = (form)=>{
    const url = `${config.baseUrl}/account/registration`
    return axios.post(url, form)
}


const userLogin = (form)=>{
    const url = `${config.baseUrl}/account/login`
    return axios.post(url, form)
}

const getUser = ()=>{
    let user = localStorage.getItem('user')
    if(user !==undefined){
        user = JSON.parse(user)
        return user
      }
    
}




function isLoggedIn(){
    let user = getUser()
    if(user){
      return true

    }
    return false
    
}


function changePassword(data){
   const user = getUser()
   console.log(user.id)
    const url = `${config.baseUrl}/account/${user.id}/change-password`
    axios.put(url, data, {headers: AuthInterceptor()})
}
function logout(){
    const user = getUser()
    if(user){
        localStorage.removeItem('user')
    }
}

const AuthService = {
    userSignup,
    userLogin,
    getUser,
    isLoggedIn,
    logout,
    changePassword

}


export default AuthService
