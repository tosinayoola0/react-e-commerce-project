import React, {useEffect} from 'react'
import {
    AccountCircleIcon, 
    RemoveIcon, 
    Divider, 
    TableChart, 
    LogoutIcon,
    FavoriteBorderIcon
} from '../icons/icons'
import Style from './sidebarStyle.module.css'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'

function Sidebar( ) {
    const state = useSelector((state)=> state.authSlice.user)
    useEffect(()=>{
    }, [])
  return (

    <div className={`d-flex flex-column justify-content-between p-4 vh-90  ${Style.container}`}>
        <div className={`${Style.sidebar}`}>
        <ul className={`nav nav-pills flex-column p-0 m-0`}>
            <li className={`nav-items p-1`}>
                <Link to={`/user/${state.id}/account-detail`} className={`nav-link text-black`}>
                    <AccountCircleIcon className={`me-2 fs-5`}/>
                    <span>My Account</span>
                </Link>
            </li>
            <Divider className={Style.divider}/>
            <li className={`nav-items p-1`}>
                <Link to={`/user/${state.id}/order-list`} className={`nav-link text-black`}>
                    <TableChart className={`me-2 fs-5`}/>
                    <span>Orders</span>
                </Link>
            </li>
            <Divider/>

            <li className={`nav-items p-1`}>
                <Link to={`/user/${state.id}/saved-items`} className={`nav-link text-black`}>
                    <FavoriteBorderIcon className={`me-2 fs-5`}/>
                    <span>Saved Items</span>
                </Link>
            </li>
            <Divider/>

            <li className={`nav-items p-1`}>
                <Link to={`/user/${state.id}/profile-update`} className={`nav-link text-black`}>
                    <span>Account Management</span>
                </Link>
            </li>
            <Divider/>

           

            <li className={`nav-items p-1`}>
                <Link className={`nav-link text-white`}>
                <RemoveIcon className={`me-2 fs-5`}/>
                    <span>Close</span>
                </Link>
            </li>

        </ul>
        </div>
        <div>
            <Divider/>
            <i>
                <LogoutIcon className={`me-2 fs-5`}/>
                <span>Logout</span>
            </i>
        </div>
    </div>
  )
}

export default Sidebar