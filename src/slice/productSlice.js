import dataService from "../services/data/dataService";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {setMessage} from './msg'


export const NewProductSlice = createAsyncThunk(
    'products/add-new-item',
    async(form, thunkAPI)=>{
        try{
            const response = await dataService.CreateProduct(form)
            thunkAPI.dispatch(setMessage(response.message))
            return response.data
        }
        catch(err){
            const msg = (
                err.response &&
                err.data.msg &&
                err.response.data.msg

            ) || err.message ||
            err.toString()
            thunkAPI.dispatch(setMessage(msg))
            return thunkAPI.rejectWithValue()

        }
    }
)

export const displayProductList = createAsyncThunk(
    'products/display-product-list',
    async(thunkAPI)=>{
        try{
            const response = await dataService.displayProductList()
            return response.data
        }
        catch(err){
            const msg = (
                err.response && err.response.msg &&
                err.response.data.msg
            ) || err.message || err.toString()
                thunkAPI.dispatch(setMessage(msg))
                return thunkAPI.rejectWithValue()
        }
    }
)

export const displayProductDetail = createAsyncThunk(
    'product/display-product-detail',
    async(id, thunkAPI)=>{
        try{
            const response = await dataService.getProductDetail(id)
            thunkAPI.dispatch(setMessage(response.data.message))
            return response.data
        }
        catch(err){
            const msg = (
                err.response && err.response.msg &&
                err.response.data.msg
            ) || err.message || err.toString()
            thunkAPI.dispatch(setMessage(msg))
            return thunkAPI.rejectWithValue()
        }
    }
)
export const  updateProductDetail = createAsyncThunk(
    'product/update-product',
    async(form, thunkAPI)=>{
        try{
            const response = await dataService.updateProduct(form.id, form)
            thunkAPI.dispatch(setMessage(response.data.message))
            return response.data
        }
        catch(err){
            const msg = (
                err.response && err.response.msg &&
                err.response.data.msg
            ) || err.message || err.toString()
            thunkAPI.dispatch(setMessage(msg))
            return thunkAPI.rejectWithValue()
        }
    }   
)

export const deleteProduct = createAsyncThunk(
    'product/delete-product',
    async(id, thunkAPI)=>{
        try{
            const response = await dataService.deleteProduct(id)
            thunkAPI.dispatch(setMessage(response.data.msg))
            return response.data
        }
        catch(err){
            const msg = (
                err.response && err.response.msg &&
                err.response.data.msg
            ) || err.message || err.toString()
            thunkAPI.dispatch(setMessage(msg))
            return thunkAPI.rejectWithValue()
        }
    }
)


export const manufacturerDetail = createAsyncThunk(
    'manufacturer/detail',
    async(id, thunkAPI)=>{
        try{
            const response = await dataService.displayotherProductsByManufacturer(id)
            thunkAPI.dispatch(setMessage(response.msg))
            return response.data
        }
        catch(err){

            const msg = (
                err.response && err.response.msg &&
                err.response.data.msg
            ) || err.message || err.toString()
            thunkAPI.dispatch(setMessage(msg))
            return thunkAPI.rejectWithValue()
        }
    }
)

let initialState = {
    product: [],
    manufacturer:[]
}

const ProductSlice = createSlice({
    name: 'product',
    initialState,
    extraReducers: {
        [NewProductSlice.fulfilled]:(state, action )=>{
            return Object.assign({}, state, {
                product: state.product.concat(action.payload)
            })
        },
        [NewProductSlice.rejected]: (state, action)=>{
            return Object.assign({}, state, {
                product: state.product
            })
        },

        [displayProductList.fulfilled]:(state, action)=>{
            return Object.assign({}, state, {
                product: state?.product?.concat(action?.payload)
            })
        },
        [displayProductList.rejected]: (state, action)=>{
            return Object.assign({}, state, {
                product : state.product
            })
        },
        [updateProductDetail.fulfilled]:(state, action)=>{
            return Object.assign({}, state, {
                product: state.product.concat(action.payload)
            })
        },
        [updateProductDetail.rejected]:(state, action)=>{
            return Object.assign({}, state,{
                product: state.product
            })
        },
        [displayProductDetail.fulfilled]:(state, action)=>{
            
            return Object.assign({}, state, {
                product: state.product.concat(action.payload)
            })
        },
        [displayProductDetail.rejected]: (state, action)=>{
            return Object.assign({}, state, {
                product: state.product
            })
        },
        [deleteProduct.fulfilled]: (state, action)=>{
            return Object.assign({}, state, {
                product: state.product.concat(action.payload)
            })
        },
        [deleteProduct.rejected]: (state, action)=>{
            return Object.assign({}, state, {
                product: state.product
            })
        },
        [manufacturerDetail.fulfilled]:(state, action)=>{
            return Object.assign({}, state, {
                manufacturer: state.manufacturer?.concat(action?.payload)
            })
        },
        [manufacturerDetail.rejected]: (state, action)=>{
            return Object.assign({}, state, {
                manufacturer: state.manufacturer
            })
        }

    } 

})


const {reducer} = ProductSlice
export default reducer