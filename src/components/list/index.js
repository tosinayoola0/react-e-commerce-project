import React, {useState, useEffect} from "react";
import { useDispatch } from "react-redux";
import { displayProductList } from "../../slice/productSlice";
import ListStyle from './index.module.css'
import { config } from "../../environment/env";
import { Link } from "react-router-dom";


const ProductList = ()=>{
    useEffect(()=>{
        getProductList()
    },[])

    const dispatch = useDispatch()
    const[genItem, setGenItems] = useState([])
    const [foodItem, setFoodItems] = useState([])
    const [electricalItem, setElectricalItem] = useState([])
    const [computerItem, setComputerItems] = useState([])
    const [clothItems, setClothItems] = useState([])


    function getProductList(){ 
        let foods= []
        let clothes=[]
        let computers=[]
        let genItem =[]
        let electri = []
        dispatch(displayProductList())
        .unwrap()
        .then((res)=>{
            let response = res
            response.forEach((x)=>{
                if(x.category === 'foods' || x.category === 'Foods'){
                    foods.push(x)

                    setFoodItems(foods)
                }
                else if(x.category==='Computer' || x.category === 'computer'){
                    computers.push(x)
                    setComputerItems(computers)
                }
                else if(x.category === 'Clothes' || x.category=== 'clothes'){
                    clothes.push(x)
                    setClothItems(clothes)
                }
                else if(x.category === 'electronics' || x.category === 'Electronics'){
                    electri.push(x)
                    setElectricalItem(electri)
                }else{
                    genItem.push(x)
                    setGenItems(genItem)
                }
            })

        })

    }


    return (
       
        <div className={`container ${ListStyle.container}`}>
            <div className={`row `}>
                <div className={` ${ListStyle.header1}`}>
                    <h1 className={ListStyle.title}>Today's Deal</h1>
                    <p className={`${ListStyle.see}`}>SEE ALL </p>
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {genItem?.map(product=>{
                        return <div  className={`${ListStyle.items} ` } key={product.id}>
                           <Link to={`/product/${product.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img className={ListStyle.img}  alt="product-img" src={`${config.imgUrl}${product.img}`}/>
                                </div>
                                <div  className={`${ListStyle.productname}`}>
                                    <p>{product?.name}</p>
                                    <p>N{product?.price}</p>
                                </div>
                           </Link>
                                    
                        </div>
                                    
                        })[0]
                    }
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {genItem?.map((product)=>{
                    return <div key={product.id} className={`${ListStyle.items}` }>
                        <Link to={`/product/${product.id}/detail`} className={`${ListStyle.link}`}>
                            <div className={`${ListStyle.img_div}`}>
                                <img className={`${ListStyle.img}`} src={`${config.imgUrl}${product.img}`} alt="product-img"/>
                            </div>
                            <div  className={`${ListStyle.productname}`}>
                                <p>{product?.name}</p>
                                <p>N{product?.price}</p>
                            </div>
                        </Link>
                            

                    </div>                     
                    })[1]}
                </div>
                
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {genItem?.map((product)=>{
                        return <div key={product.id} className={`${ListStyle.items}` }>
                            <Link to={`/product/${product.id}/detail`} className={`${ListStyle.link}`}>
                            
                                <div className={`${ListStyle.img_div}`}>
                                <img className={`${ListStyle.img}`} src={`${config.imgUrl}${product.img}`}  alt="product-img"/>
                                </div>
                                <div  className={`${ListStyle.productname}`}>
                                    <p>{product?.name}</p>
                                    <p>N{product?.price}</p>
                                </div>
                            </Link>
                        </div>                     
                    })[2]}      
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {genItem?.map((product)=>{
                        return <div key={product.id} className={`${ListStyle.items}` }>
                            <Link to={`/product/${product.id}/detail`} className={`${ListStyle.link}`}>
                            
                                <div className={`${ListStyle.img_div}`}>
                                <img className={`${ListStyle.img}`} src={`${config.imgUrl}${product.img}`}  alt="product-img"/>
                                </div>
                                <div  className={`${ListStyle.productname}`}>
                                    <p>{product?.name}</p>
                                    <p>N{product?.price}</p>
                                </div>
                            </Link>
                        </div>                     
                    })[3]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {genItem?.map((product)=>{
                        return <div key={product.id} className={`${ListStyle.items}` }>
                            <Link to={`/product/${product.id}/detail`} className={`${ListStyle.link}`}>
                            
                                <div className={`${ListStyle.img_div}`}>
                                <img className={`${ListStyle.img}`} src={`${config.imgUrl}${product.img}`}  alt="product-img"/>
                                </div>
                                <div  className={`${ListStyle.productname}`}>
                                    <p>{product?.name}</p>
                                    <p>N{product?.price}</p>
                                </div>
                            </Link>
                        </div>                     
                    })[4]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {genItem?.map((product)=>{
                        return <div key={product.id} className={`${ListStyle.items}` }>
                            <Link to={`/product/${product.id}/detail`} className={`${ListStyle.link}`}>
                            
                                <div className={`${ListStyle.img_div}`}>
                                <img className={`${ListStyle.img}`} src={`${config.imgUrl}${product.img}`}  alt="product-img"/>
                                </div>
                                <div  className={`${ListStyle.productname}`}>
                                    <p>{product?.name}</p>
                                    <p>N{product?.price}</p>
                                </div>
                            </Link>
                        </div>                     
                    })[5]}
                </div>
            </div>
            
            <div className={`row`}>
                
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {genItem?.map((product)=>{
                        return <div key={product.id} className={`${ListStyle.items}` }>
                            <Link to={`/product/${product.id}/detail`} className={`${ListStyle.link}`}>
                            
                                <div className={`${ListStyle.img_div}`}>
                                <img className={`${ListStyle.img}`} src={`${config.imgUrl}${product.img}`}  alt="product-img"/>
                                </div>
                                <div  className={`${ListStyle.productname}`}>
                                    <p>{product?.name}</p>
                                    <p>N{product?.price}</p>
                                </div>
                            </Link>
                        </div>                     
                    })[6]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {genItem?.map((product)=>{
                        return <div key={product.id} className={`${ListStyle.items}` }>
                            <Link to={`/product/${product.id}/detail`} className={`${ListStyle.link}`}>
                            
                                <div className={`${ListStyle.img_div}`}>
                                <img className={`${ListStyle.img}`} src={`${config.imgUrl}${product.img}`}  alt="product-img" />
                                </div>
                                <div  className={`${ListStyle.productname}`}>
                                    <p>{product?.name}</p>
                                    <p>N{product?.price}</p>
                                </div>
                            </Link>
                        </div>                     
                    })[7]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {genItem?.map((product)=>{
                        return <div key={product.id} className={`${ListStyle.items}` }>
                            <Link to={`/product/${product.id}/detail`} className={`${ListStyle.link}`}>
                            
                                <div className={`${ListStyle.img_div}`}>
                                <img className={`${ListStyle.img}`} src={`${config.imgUrl}${product.img}`}  alt="product-img"/>
                                </div>
                                <div  className={`${ListStyle.productname}`}>
                                    <p>{product?.name}</p>
                                    <p>N{product?.price}</p>
                                </div>
                            </Link>
                        </div>                     
                    })[8]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {genItem?.map((product)=>{
                        return <div key={product.id} className={`${ListStyle.items}` }>
                            <Link to={`/product/${product.id}/detail`} className={`${ListStyle.link}`}>
                            
                                <div className={`${ListStyle.img_div}`}>
                                <img className={`${ListStyle.img}`}  alt="product-img" src={`${config.imgUrl}${product.img}`} />
                                </div>
                                <div  className={`${ListStyle.productname}`}>
                                    <p>{product?.name}</p>
                                    <p>N{product?.price}</p>
                                </div>
                            </Link>
                        </div>                     
                    })[9]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {genItem?.map((product)=>{
                        return <div key={product.id} className={`${ListStyle.items}` }>
                            <Link to={`/product/${product.id}/detail`} className={`${ListStyle.link}`}>
                            
                                <div className={`${ListStyle.img_div}`}>
                                <img className={`${ListStyle.img}`}  alt="product-img" src={`${config.imgUrl}${product.img}`}/>
                                </div>
                                <div  className={`${ListStyle.productname}`}>
                                    <p>{product?.name}</p>
                                    <p>N{product?.price}</p>
                                </div>
                            </Link>
                        </div>                     
                    })[10]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {genItem?.map((product)=>{
                        return <div key={product.id} className={`${ListStyle.items}` }>
                            <Link to={`/product/${product.id}/detail`} className={`${ListStyle.link}`}>
                            
                                <div className={`${ListStyle.img_div}`}>
                                <img className={`${ListStyle.img}`}  alt="product-img" src={`${config.imgUrl}${product.img}`} />
                                </div>
                                <div  className={`${ListStyle.productname}`}>
                                    <p>{product?.name}</p>
                                    <p>N{product?.price}</p>
                                </div>
                            </Link>
                        </div>                     
                    })[11]}
                </div>
            </div>
            <div className={`row `}>
               <div className={` ${ListStyle.header2}`}>
                    <h1 className={ListStyle.title}>Amazing Food for You</h1>
                    <p className={`${ListStyle.see}`}>SEE ALL </p>

                </div>
                <div className={`col--sm-12 col-lg-2 col-md-2 `}>
                    {foodItem.map((item)=>{
                        return <div key={item.id} className={`$${ListStyle.items}`}>
                            <Link to={`/product/${item.id}/detail`} className={`${ListStyle.link}`}>
                                
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${item.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div  className={`${ListStyle.productname}`}>
                                    <p>{item?.name}</p>
                                    <p>N{item.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[0]}
                </div>

                <div className={`${ListStyle.product} col-lg-2 col-sm-12 col-md-2`}>
                    {foodItem.map((item)=>{
                        return <div key={item.id} className={`$${ListStyle.items}`}>
                                <Link to={`/product/${item.id}/detail`} className={`${ListStyle.link}`}>
                                
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${item.img}`} className={`${ListStyle.img}`}  alt="product-img" />
                                </div>
                                <div  className={`${ListStyle.productname}`}>
                                    <p>{item?.name}</p>
                                    <p>N{item.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[1]}
                </div>
                <div className={`col-lg-2 col-lg-1 col-md-2`}>
                    {foodItem.map((item)=>{
                        return <div key={item.id} className={`$${ListStyle.items}`}>
                            <Link to={`/product/${item.id}/detail`} className={`${ListStyle.link}`}>
                                
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${item.img}`} className={`${ListStyle.img}`}  alt="product-img" />
                                </div>
                                <div  className={`${ListStyle.productname}`}>
                                    <p>{item?.name}</p>
                                    <p>N{item.price}</p>
                                </div>
                            </Link>
                        </div>                  
                    })[2]}
                </div>
                <div className={`${ListStyle.product} col-lg-2 col-sm-12 col-md-2`}>
                    {foodItem.map((item)=>{
                        return <div key={item.id} className={`$${ListStyle.items}`}>
                            <Link to={`/product/${item.id}/detail`} className={`${ListStyle.link}`}>
                                
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${item.img}`} className={`${ListStyle.img}`}  alt="product-img" />
                                </div>
                                <div  className={`${ListStyle.productname}`}>
                                    <p>{item?.name}</p>
                                    <p>N{item.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[3]}
                </div>
                <div className={`${ListStyle.product} col-lg-2 col-sm-12 col-md-2`}>
                    {foodItem.map((item)=>{
                        return <div key={item.id} className={`$${ListStyle.items}`}>
                            <Link to={`/product/${item.id}/detail`} className={`${ListStyle.link}`}>
                            
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${item.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div  className={`${ListStyle.productname}`}>
                                    <p>{item?.name}</p>
                                    <p>N{item.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[4]}
                </div>
                <div className={`${ListStyle.product} col-lg-2 col-sm-12 col-md-2`}>
                    {foodItem.map((item)=>{
                        return <div key={item.id} className={`$${ListStyle.items}`}>
                            <Link to={`/product/${item.id}/detail`} className={`${ListStyle.link}`}>
                            
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${item.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div  className={`${ListStyle.productname}`}>
                                    <p>{item?.name}</p>
                                    <p>N{item.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[5]}
                </div>
            </div>
            <div className={`row`}>
                <div className={` ${ListStyle.header3}`}>
                    <h1 className={ListStyle.title}>Best Electrical Gadget For You</h1>
                    <p className={`${ListStyle.see}`}>SEE ALL </p>

                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {electricalItem.map((electri)=>{
                        return <div className={`$ ${ListStyle.items}`} key={electri.id}> 
                            <Link to={`/product/${electri.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${electri.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{electri.name}</p>
                                    <p>N{electri.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[0]}
                </div>

                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {electricalItem.map((electri)=>{
                        return <div className={`$ ${ListStyle.items}`} key={electri.id}> 
                            <Link to={`/product/${electri.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${electri.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{electri.name}</p>
                                    <p>N{electri.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[1]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {electricalItem.map((electri)=>{
                        return <div className={`$ ${ListStyle.items}`} key={electri.id}> 
                            <Link to={`/product/${electri.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${electri.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{electri.name}</p>
                                    <p>N{electri.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[2]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {electricalItem.map((electri)=>{
                        return <div className={`$ ${ListStyle.items}`} key={electri.id}> 
                            <Link to={`/product/${electri.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${electri.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{electri.name}</p>
                                    <p>N{electri.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[3]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {electricalItem.map((electri)=>{
                        return <div className={`$ ${ListStyle.items}`} key={electri.id}> 
                            <Link to={`/product/${electri.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${electri.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{electri.name}</p>
                                    <p>N{electri.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[4]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {electricalItem.map((electri)=>{
                        return <div className={`$ ${ListStyle.items}`} key={electri.id}> 
                            <Link to={`/product/${electri.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${electri.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{electri.name}</p>
                                    <p>N{electri.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[5]}
                </div>
            </div>
            <div className={`row ${ListStyle.row}`}>
                <div className={` ${ListStyle.header4}`}>
                    <h1 className={ListStyle.title}>Best Computer And Accessories  For You</h1>
                    <p className={`${ListStyle.see}`}>SEE ALL </p>

                    
                </div>
                
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {computerItem.map((computer)=>{
                        return <div className={`$ ${ListStyle.items}`} key={computer.id}> 
                            <Link to={`/product/${computer.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${computer.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{computer.name}</p>
                                    <p>N{computer.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[0]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {computerItem.map((computer)=>{
                        return <div className={`$ ${ListStyle.items}`} key={computer?.id}> 
                            <Link to={`/product/${computer.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${computer?.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{computer?.name}</p>
                                    <p>N{computer?.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[1]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>

                    {computerItem.map((computer)=>{
                        return <div className={`$ ${ListStyle.items}`} key={computer.id}> 
                            <Link to={`/product/${computer.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${computer.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{computer.name}</p>
                                    <p>N{computer.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[2]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {computerItem.map((computer)=>{
                        return <div className={`$ ${ListStyle.items}`} key={computer?.id}> 
                            <Link to={`/product/${computer.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${computer?.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{computer?.name}</p>
                                    <p>N{computer?.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[3]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {computerItem.map((computer)=>{
                        return <div className={`$ ${ListStyle.items}`} key={computer?.id}> 
                            <Link to={`/product/${computer.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${computer?.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{computer?.name}</p>
                                    <p>N{computer?.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[4]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {computerItem.map((computer)=>{
                        return <div className={`$ ${ListStyle.items}`} key={computer?.id}> 
                            <Link to={`/product/${computer.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${computer?.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{computer?.name}</p>
                                    <p>N{computer?.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[5]}
                </div>
            </div>
            <div className={`row ${ListStyle.row}`}>
                <div className={`${ListStyle.header5}`} >
                    <h1 className={ListStyle.title}>Best Collections of Wears & Accessories</h1>
                    <p className={`${ListStyle.see}`}>SEE ALL </p>

                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {clothItems.map((cloth)=>{
                        return <div className={`$ ${ListStyle.items}`} key={cloth.id}> 
                            <Link to={`/product/${cloth.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img className={`${ListStyle.img}`}  alt="product-img" src={`${config.imgUrl}/${cloth.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{cloth.name}</p>
                                    <p>N{cloth.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[0]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {clothItems.map((cloth)=>{
                        return <div className={`$ ${ListStyle.items}`} key={cloth.id}> 
                            <Link to={`/product/${cloth.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${cloth.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{cloth.name}</p>
                                    <p>{cloth.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[1]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {clothItems.map((cloth)=>{
                        return <div className={`$ ${ListStyle.items}`} key={cloth.id}> 
                            <Link to={`/product/${cloth.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${cloth.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{cloth.name}</p>
                                    <p>{cloth.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[2]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {clothItems.map((cloth)=>{
                        return <div className={`$ ${ListStyle.items}`} key={cloth.id}> 
                            <Link to={`/product/${cloth.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${cloth.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{cloth.name}</p>
                                    <p>{cloth.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[3]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {clothItems.map((cloth)=>{
                        return <div className={`$ ${ListStyle.items}`} key={cloth.id}> 
                            <Link to={`/product/${cloth.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${cloth.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{cloth.name}</p>
                                    <p>{cloth.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[4]}
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2`}>
                    {clothItems.map((cloth)=>{
                        return <div className={`$ ${ListStyle.items}`} key={cloth.id}> 
                            <Link to={`/product/${cloth.id}/detail`} className={`${ListStyle.link}`}>
                                <div className={`${ListStyle.img_div}`}>
                                    <img src={`${config.imgUrl}/${cloth.img}`}  alt="product-img" className={`${ListStyle.img}`}/>
                                </div>
                                <div className={`${ListStyle.product}`}>
                                    <p>{cloth.name}</p>
                                    <p>{cloth.price}</p>
                                </div>
                            </Link>
                        </div>
                    })[5]}
                </div>
            </div>

    </div>
        
    )
}

export default ProductList