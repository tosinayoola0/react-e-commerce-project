export const Locations  =[
    {'state': {
        "name":"Abia State","id":1, 'cities': 
        [   {name: "Please Select a city "},
            {'name': 'Aba', 'id': 1, 'location':
                [
                    {
                        'id': 1, 'name': 'Aba Station', 'price': 2300, 'address': 
                        'N0. 49 Okigwe Road Aba, opposite Devin Grace Comprehensive school Tonimas filling station Aba, Abia', 
                        'opening_time': 'Mon - Fri 8am - 7pm; Sat 8am - 7pm'
                }
                ]
            }, 
            {'name': 'Umuahia', 'id':2, 'location': 
                [
                    {
                        'name':'Ohafia Station', 'address': 'No.13 Ohafia Street, Umuahia Abia State Behind Slot house Umuahia, Abia', 'price': 2300, 
                        'opening_time': 'Mon-Fri 8am-7pm ; Sat 8am-7pm'
                    } 
                ]
            }
        ]}
    }, 
    {
        'state':{"name":"Akwa Ibom State","id":2, 'cities': 
            [
               {"name": "Please Select a city "},
                {'name':'Uyo', 'id': 1, 'location': 
                    [
                        {
                            'name':'Ibom Station', 'price': 2000, 
                            'address': 'No. 1 Dr. Clement Isong Street, Off Abak Road St. Athanasius Hospital, Uyo Uyo, Akwa Ibom', 
                        '   opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm'
                        }
                    ]
                }
            ]
        },
    }, 
    {
    'state':{"name":"Anambra State","id":3, 'cities': 
        [   
            {"name": "Please Select a city "},
            {'name': 'Amawbia/Awka', 'id': 1, 'location': 
                [
                    {
                        'name': 'Amobia Station', 
                        'price':  2300,
                        'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                        'address': 'No, 4 lamitel crescent behind Kings Merit hotel, temp site Awka, Anambra Behind Kings Merit hotel AMAWBIA / AWKA, Anambra'
                    }
                ]
            },
            {
                'name': 'Akwa Town', 'id': 2, 'location':
               [
                    {
                        'name':'Aroma Station',
                        'price':  2300,
                        'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                        'address':`Ezeokoye Chukwunonso Street, Crunchies Fast-food junction Awka, behind FCMB bank, along Enugu-Onitsha express road Awka, Anambra state.
                        Crunchies Fast-food Awka AWKA TOWN, Anambra`

                    }
                ]
            },
            {
                'name': 'Nnewi-Nnobi', 'id':3, 'location':
                [
                    {
                        'name': 'Nnobi Station',
                        'price': 2300,
                        'address': `opposite Alabama's house before Urugate Nnewi. Behind Anaedo hotel Nnewi -Nnobi, Anambra`,
                        'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                        
                    }
                ]   
            },
            {
                'name': 'Onitsha-Central Location', 'id': 4, 'location': 
                [
                    
                    {
                    'name':'Nkpor Station',
                    'price':  2300,
                    'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                    'address': `123 Limca road, Onitsha besides SUSU College Susu College ONITSHA-CENTRAL LOCATIONS, Anambra`
                    }
                ]
            }
        ]},
    }, 
    {
        'state': { 'name': "Bauchi", 'id': 4, 'cities':
             [
                {"name": "Please Select a city "},
                {
                    'name': 'Wunti', 'id': '1', 'location': [
                        {
                            'name': 'Bauchi Station',
                            'price':  3100,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address':" No. 73 Baraya plaza, Opposite Baraya mosque, Gwabba Nassarawa street. Bauchi State. Opposite Baraya mosque Wunti, Bauchi"
                        }
                    ]
                 }   
            ]
         }
    },
    {
        'state':{"name":"Cross River State","id":5, 'cities': 
            [
                {"name": "Please Select a city "},
                {
                    'name': 'Calabar', 'id': 1, 'location':
                    [
                        {
                            'name':'PDC Calabar Ishie Town Station',
                            'price':  2300,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address':  'O.1, NSEMO STREET, OFF OLD ODUKPANI ROAD, IKOT EKAN EDEM, ISHIE TOWN PIONEER GUEST HOUSE Calabar, Cross River'
                        },
                        {
                            'name': 'PDC State Housing Ikot Omin Station',
                            'price':  2300,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address':`207 Murtala Muhammed Highway, 8 miles junction Calabar. Beside Peggy's Supermarket Calabar, Cross River`

                        },
                        {
                            'name': 'State Housing Station',
                            'price':  2300,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': '8 Effange Mkpa street, by state housing police station Calabar Cross river state Opp.state housing police station lock-up shops Calabar, Cross River'
                            
                        }
                    ]
                }        
            ]
        
        },

    },
    {
        'state':{"name":"Delta State","id":6, 'cities':[
            {"name": "Please Select a city "},
            {
                'name':'Asaba-GRA core area', 'id': 1, 'location': 
                    [
                        {
                            'name': 'Asaba Pickup Station',
                            'price':  2300,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': 'No 5, Beatrice Iyamu Nwayindudu Street,Off Ogagifo street,Opposite former Deputy Governors Office ,DBS road, Asaba,Delta State Deputy Governor\'S Office,Asaba ASABA-GRA core area, Delta'
                        }        
                    ]
            },
            {
                'name': 'Warri Airport Road', 'id':2, 'location': 
                [
                    {
                        'name':'Warri Pickup Station',
                        'price':  2300,
                        'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                        'address':'PLOT 11, 37 STREET DDPA OFF WEST-END HOSPITAL ROAD, OFF AIRPORT ROAD WARRI DELTA STATE. Off West-End Road, Off Airport Road WARRI-AIRPORT ROAD, Delta'
                    }
                ]
            }

        ]}, 
    },
    {
        'state': {'name': 'Ebonyi', 'id': 7, 'cities':
            [
                {"name": "Please Select a city "},
                {
                    'name':'Abakali', 'id': 1, 'location':[
                        {
                            'name': 'Ebonyi Station',
                            'price':  2600,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address':'7 Nsugbe Street, Abakaliki, Ebonyi State. Water works Road, Before Denco Hotel ABAKALIKI, Ebonyi'

                        }
                    ]

                }
            ]
        }
    },
    {
        'state': {
            'name': 'Edo', 'id':8, 'cities': [
                {"name": "Please Select a city "},
                {
                    'name': 'Benin GRA', 'id': 1, 'location': 
                    [
                        {
                            'name':'Benin Pickup Station',
                            'price':  2600,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address':'plot 108 Namuna Street, Off Limit Way, Off Sapele road benin city Grand Foundation Group Of Schools Benin-GRA, Edo'        
                        },
                        {   
                            'name': 'Auchi Station',
                            'price':  2600,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': 'MESHI BUILDING OPPOSITE LEVIS FUEL STATION SABO SOUTH CDA AUCHI EDO STATE SABO POLICE STATION Etsako-Auchi Poly Campus 1, Edo'
                        }
                    ]
    
                }
            ]
        }
    },
    {
        'state':{ 'name': 'Ekiti', 'id': 9, 'cities': [
            {"name": "Please Select a city "},
            {
                'name': 'Ado Ekiti', 'id': 1, 'location':
                [
                    {
                        'name': 'Ekiti Two Station',
                        'price':  2300,
                        'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                        'address': '7 Adewumi Fajana Avenue, Ekiti State Housing Estate Phase 1, Oke Ila, Ado Ekiti, \
                        Ekiti State Housing Estate Phase 1 Gate by Vigilante Group of Nigeria office Ado Ekiti, Ekiti'
                    },
                    {   
                        'name': 'Jumia Pickup Station IKERE ADO',
                        'price':  2300,
                        'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                        'address': 'No 25 Ikere Road, Adjacent Little by Little Plaza,Ikere Road, Ajilosun Ado Ekiti\
                        LITTLE BY LITTLE PLAZA Ado Ekiti, Ekiti'
                    }

                ] 
            }
        ]}
    },
    {
        'state': {
            'name': 'Enugu', 'id': 10, 'cities': 
            [
                {"name": "Please Select a city "},
                { name: 'Enugu Central Location', 'id': 1, 'location': 
                    [
                        {
                            'name': 'New Haven Station',
                            'price':  2300,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address':'13 Mbanefo Street, off Down Chime Road , New Haven, Enugu State. First Bank Enugu-Central Location, Enugu'
                        }
                    ]
                }
            ]
        }
    },
    {
        'state': {
            'name': 'Gombe', 'id':11, 'cities': [
                {"name": "Please Select a city "},
                {
                    'name':'GRA', 'id': 1, 'location': [
                        {
                            'name': 'Gombe Station',
                            'price':  3100,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': 'Suite A7 & A8 Ammi Plaza, Opposite Bauchi Motor Park, Bauchi - Gombe Road, Gombe State Bauchi Motor Park GRA, Gombe'
                        
                        }
                    ]
                }
            ]
        }
    },
    {
        'state':{'name':   'Imo State', 'id': 12, 'cities':
            [
                {"name": "Please Select a city "},
                {
                    'name': 'Amakohia-Akwakuma', 'id':1, 'location': [
                        {
                            'name': 'PDC Ikenegbu Amakohia Akwakuma Station',
                            'price':  2600,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': 'Powa Plaza(Mammy) Opp.OH Yes! Fast Food By Rapour Junction Amakohia Akwakuma Orlu Road Owerri Imo state Rapour Hotel Junction AMAKOHIA-AKWAKUMA, Imo'
                        },
                    ] 
                },
                {
                    'name': 'Owerri-Wetheral', 'id':2, 'location': [
                        {
                            'name': 'PDC Ikenegbu Douglas Station',
                            'price':  2600,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': '118 Wetheral Road, Opposite First Baptist Church, Fireservice junction, Owerri. Baptist Church Owerri-Wetheral, Imo'
                        },
                    ] 
                }
            ]
        }
    },
    {
        'state': {
            'name': "Federal Capital Territory", 'id': 13, 'cities': 
            [
                {"name": "Please Select a city "},
                {
                    'name': 'Gwarinpa 1st Avenue', 'id': 1,'location': 
                    [ 
                        {
                            'name': 'Gwarinpa Pickup Station',
                            'price':  1600,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': 'Suite A16, Gostu Plaza, Beside Oando Filling Station abuja \
                            Beside Oando Filing Station ABUJA- GWARINPA 1ST AVENUE, Federal Capital Territory'                  
                        },
                        
                        
                    ]
                },

                {
                    'name': 'Garki Area 11', 'id': 1,'location': 
                    [ 
                        {
                            'name': 'Melita Pickup Station',
                            'price':  1800,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': 'Melita Plaza Suite, D2 , Plot 599, Gwarjo Close, Off Gimbiya Street, Area II Beside Area 11 Shopping Mall\
                            ABUJA- GARKI AREA 11, Federal Capital Territory'                  
                        },
                        
                        
                    ]
                },
                
                {
                    'name': 'Gwagwalada', 'id': 3, 'location': 
                    [
                        {
                            'name': 'Gwagwalada Pickup Station',
                            'price':  1800,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address':'Suite B11, Deo Shopping Mall, Market Road. Plot 473 Gwagwalada Expansion Layout, Gwagwalada.\
                            Opposite Gwagwalada Market and Close To First Bank Market Road GWAGWALADA, Federal Capital Territory'
                        }
                    ]
                }            
            ]
        }
    },

    {
        'state': {
            'name': "Lagos", 'id': 14, 'cities': 
            [
                {"name": "Please Select a city "},
                {
                    'name': 'Lekki-Ajah (Ilaje)', 'id': 1,'location': 
                    [ 
                        {
                            'name': 'Abraham Adesanya Station',
                            'price':  800,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': 'No 2, Feyitola Street, Balogun Town, Sangotedo, Lekki-Epe Expressway, Lagos. Fara Park, Sangotedo\
                            LEKKI-AJAH (ILAJE), Lagos'                  
                        },
                        {
                            'name': 'NG-Scheme2-Station',
                            'price':  800,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': '3B Adesola Onafawora Close, Road 6A Lekki Scheme 2. Lagos. Abraham Adesanya Estate\
                            LEKKI-AJAH (ILAJE), Lagos'                  
                        },
                        
                        
                    ]
                },
                {
                    'name':'Ikeja (Opebi)', 'id': 2, 'location': 
                    [
                        {
                            'name': 'Ikeja Pick-Up Station',
                            'price':  1800,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': 'No 1 Regina Omolara street Off Agbaoku, Ikeja, Lagos Tasty Fried Chicken\
                            Ikeja (OPEBI), Lagos'
                        }
                    ]
                },                

                {
                    'name': 'Badagry', 'id': 3, 'location': 
                    [
                        {
                            'name': 'Gwagwalada Pickup Station',
                            'price':  2300,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address':'Block A2, HSE 1. Muhammad Buhari Estate Badagry, Lagos State. Muhammad Buhari Estate Bus stop\
                            Badagry, Lagos'
                        }
                    ]
                },
                {
                    'name': 'Abule Egba(Meiran Road)', 'id': 4, 'location': 
                    [
                        {
                            'name': 'Abule Egba(Meiran Road) Pickup Station',
                            'price':  800,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address':'No 3 Felix onabanjo street, off lawal unity, meiran road Ile Iwe Bus stop, Meiran Model College\
                            Abule Egba (Meiran Road), Lagos'
                        }
                    ]
                },
                {
                    'name': 'Festac(1st Avenue)', 'id': 5, 'location': 
                    [
                        {
                            'name': 'Festac(1st Avenue) Pickup Station',
                            'price':  800,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address':'1st Avenue Opposite L close Festac Town Beside Catholic Church of Visitation\
                            FESTAC (1st Avenue), Lagos'
                        }
                    ]
                }            
            ]
        }
    },
    {
        'state':{'name':   'Plateau State', 'id': 15, 'cities':
            [
                {"name": "Please Select a city "},
                {
                    'name': 'Jos North', 'id':1, 'location': [
                        {
                            'name': 'Jos Pickup Station',
                            'price':  3100,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': 'Carin Business Centre, 24 Beach Road, Jos - Plateau State Post Office\
                            Ahmadu Bello Way, Plateau'
                        },
                    ] 
                },
            ]
        }
    },
    {
        'state':{'name':   'Rivers State', 'id': 16, 'cities':
            [
                {"name": "Please Select a city "},
                {
                    'name': 'Portharcourt-Alakahia-UPTH', 'id':1, 'location': [
                        {
                            'name': 'PDC Eliozu Alakahia Station',
                            'price':  3000,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': 'Franklin Plaza,  3 East west Road, Alkahia, Portharcourt. Rivers State Opposite UPTH Junction\
                            PORTHARCOURT-ALAKAHIA-UPTH, Rivers'
                        },
                    ] 
                },
                {
                    'name': 'Portharcourt- Staduim Road', 'id':2, 'location': [
                        {
                            'name': 'Phc-Fedex Station',
                            'price':  3000,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': 'Plot 192 Stadium Road by mummy B Junction Portharcourt River state Mummy B Junction Stadium Road\
                            PORTHARCOURT-STADIUM ROAD, Rivers'
                        },
                    ] 
                }
                
            ]
        }
    },
    {
        'state':{'name':   'Kano State', 'id': 17, 'cities':
            [
                {"name": "Please Select a city "},
                {
                    'name': 'Farm Center', 'id':1, 'location': [
                        {
                            'name': 'PDC Hotoro-Farm Centre Station',
                            'price':  3100,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': 'Shop B3 & B4, No. 17 Rahama Plaza, Justice Dahiru Mustapha Road, Farm Center - Tarauni Local Government, Kano State Farm Centre\
                            FARM CENTRE, Kano'
                        },
                    ] 
                },
                {
                    'name': 'Hotoro', 'id':2, 'location': [
                        {
                            'name': 'Hotoro Station',
                            'price':  3100,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': 'Shop No. 4, Shehu Kazaure Road, Off Maiduguri Road, Opposite Kwanan Maggi, Hotoro GRA – Kano State. Kwanan Maggi\
                            HOTORO, Kano'
                        },
                    ] 
                }
                
            ]
        }
    },
    {
        'state':{'name':   'Kaduna State', 'id': 18, 'cities':
            [
                {"name": "Please Select a city "},
                {
                    'name': 'Barnawa', 'id':1, 'location': [
                        {
                            'name': 'Kaduna Pickup Station',
                            'price':  3100,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': 'N0. 50 CMBU ROAD, OFF YUSUF ILIYA STREET, BARNAWA GRA, KADUNA Street Behind Invicta Radio\
                            BARNAWA, Kaduna'
                        },
                    ] 
                },
                {
                    'name': 'Zaria', 'id':2, 'location': [
                        {
                            'name': 'Zaria Pickup Station',
                            'price':  3100,
                            'opening_time': 'MON- FRI 8am - 7pm; SAT 8am- 7pm',
                            'address': 'Shop B10 & B11 Hulhude Plaza Sokoto Road, Randan Kano. Opp First Prestige Filling Station\
                            ZARIA, Kaduna'
                        },
                    ] 
                }
                
            ]
        }
    },


    
]
