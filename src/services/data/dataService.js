import axios from "axios";
import { config } from "../../environment/env";
import  AuthInterceptor  from "../../interceptor/auth";


const  CreateProduct = (form)=>{
    const url = `${config.baseUrl}/products/add-product`
    return axios.post(url, form, {headers: AuthInterceptor()})
}

const displayProductList=()=>{
    const url = `${config.baseUrl}/products/list`
    return axios.get(url)
}

const getProductDetail=(id)=>{
    const url = `${config.baseUrl}/products/product-detail/${id}`
    return axios.get(url)
}


const updateProduct =(id, form)=>{
    const url = `${config.baseUrl}/products/update-product/${id}`
    return axios.put(url, form, {headers: AuthInterceptor()})
}


const deleteProduct = (id)=>{
    const url = `${config.baseUrl}/products/delete-product/${id}`
    return axios.delete(url, {headers:AuthInterceptor()})
}

const createCategory = (form)=>{
    const url = `${config.baseUrl}/products/add-category`
    return axios.post(form, {headers: AuthInterceptor()})
}

const updateCategory = (id, form) =>{
    const url = `${config.baseUrl}/products/update-category/${id}`
    return axios.put(url, form, {headers:AuthInterceptor()})
}

const deleteCategory = (id) =>{
    const url = `${config.baseUrl}/products/delete-category/${id}`
    return axios.delete(url, {headers:AuthInterceptor()})
}

const listCategory = ()=>{
    const url = `${config.baseUrl}/products/list-category`
    return axios.get(url)
}

const displayCategoryById = (id) =>{
    const url =    `${config.baseUrl}/products/category-detail/${id}`
    return axios.get(url)
}

const createManufacturer = (form)=>{
    const url = `${config.baseUrl}/products/add-manufacturer`
    return axios.post(form, {headers: AuthInterceptor()})
}

const manufacturerDetail = (id)=>{
    const url = `${config.baseUrl}/product/manufacturer-detail/${id}`
    return axios.get(url)
}
const updateManufacturer = (id, form) =>{
    const url = `${config.baseUrl}/products/update-manufacturer/${id}`
    return axios.put(url, form, {headers:AuthInterceptor()})
}

const deleteManufacturer = (id) =>{
    const url = `${config.baseUrl}/products/delete-manufacturer/${id}`
    return axios.delete(url, {headers:AuthInterceptor()})
}

const listManufacturer = ()=>{
    const url = `${config.baseUrl}/products/list-manufacturer`
    return axios.get(url)
}

const displayotherProductsByManufacturer = (id) =>{
    const url = `${config.baseUrl}/products/manufacturer-detail/${id}`
    return axios.get(url)
}



const DataService = {
    CreateProduct,
    displayProductList,
    deleteProduct,
    updateProduct,
    getProductDetail,
    updateCategory, 
    createCategory,
    listManufacturer,
    displayCategoryById,
    displayotherProductsByManufacturer,
    deleteManufacturer,
    updateManufacturer,
    listCategory,
    createManufacturer,
    deleteCategory,
    manufacturerDetail
}

export default DataService