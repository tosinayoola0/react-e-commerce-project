import React, {useEffect, useState} from "react";
import { useDispatch } from "react-redux";
import { config } from "../../environment/env";
import { Link } from "react-router-dom";
import { manufacturerLists } from "../../slice/manufacturerSlice";
import Style from './manufacturer-list.module.css'


const ManufacturerList = ()=>{
    const [manufacturerList, setList] = useState([])
    const dispatch = useDispatch()
    
    
    
    useEffect(()=>{
        getManufacturerList()
    }, [])


    function getManufacturerList(){
        dispatch(manufacturerLists())
        .unwrap()
        .then((res)=>{
            setList(res)
            //console.log(manufacturerList)
        })
    }


    return (
        <div className={`${Style.container} container`}>
            <div className={`${Style.title_div}`}>
                <h1 className={`${Style.title}`}>Offical Store</h1>
            </div>
            <div className="row">
                <div className={`col-sm-12 col-lg-2 col-md-2 ${Style.list}` }>
                    {manufacturerList.map((item)=>{ 
                        return <div className={`${Style.items} `}>    
                            <img className={`${Style.img}`} alt="manufacturere-logo" src={`${config.imgUrl}${item.img}`}/>
                        </div>

                        })[0]
                    }
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2 ${Style.list}` }>
                    {manufacturerList.map((item)=>{ 
                        return <div className={`${Style.list} `}>    
                            <img className={`${Style.img}`} alt="manufacturere-logo" src={`${config.imgUrl}${item.img}`}/>

                        </div>

                        })[1]
                    }
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2 ${Style.list}` }>
                    {manufacturerList.map((item)=>{ 
                        return <div className={`${Style.list} `}>    
                            <img className={`${Style.img}`} alt="manufacturere-logo" src={`${config.imgUrl}${item.img}`}/>
                        </div>

                        })[2]
                    }
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2 ${Style.list}` }>
                    {manufacturerList.map((item)=>{ 
                        return <div className={`${Style.list} `}>    
                            <img className={`${Style.img}`} alt="manufacturere-logo" src={`${config.imgUrl}${item.img}`}/>
                        </div>

                        })[3]
                    }
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2 ${Style.list}` }>
                    {manufacturerList.map((item)=>{ 
                        return <div className={`${Style.list} `}>    
                            <img className={`${Style.img}`} alt="manufacturere-logo" src={`${config.imgUrl}${item.img}`}/>
                        </div>

                        })[4]
                    }
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2 ${Style.list}` }>
                    {manufacturerList.map((item)=>{ 
                        return <div className={`${Style.list} `}>    
                            <img className={`${Style.img}`} alt="manufacturere-logo" src={`${config.imgUrl}${item.img}`}/>
                        </div>

                        })[5]
                    }
                </div>
            </div>
            <div className="row">
                <div className={`col-sm-12 col-lg-2 col-md-2 ${Style.list}` }>
                    {manufacturerList.map((item)=>{ 
                        return <div className={`${Style.items} `}>    
                            <img className={`${Style.img}`} alt="manufacturere-logo" src={`${config.imgUrl}${item.img}`}/>
                        </div>

                        })[6]
                    }
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2 ${Style.list}` }>
                    {manufacturerList.map((item)=>{ 
                        return <div className={`${Style.list} `}>    
                            <img className={`${Style.img}`} alt="manufacturere-logo" src={`${config.imgUrl}${item.img}`}/>

                        </div>

                        })[7]
                    }
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2 ${Style.list}` }>
                    {manufacturerList.map((item)=>{ 
                        return <div className={`${Style.list} `}>    
                            <img className={`${Style.img}`} alt="manufacturere-logo" src={`${config.imgUrl}${item.img}`}/>
                        </div>

                        })[8]
                    }
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2 ${Style.list}` }>
                    {manufacturerList.map((item)=>{ 
                        return <div className={`${Style.list} `}>    
                            <img className={`${Style.img}`} alt="manufacturere-logo" src={`${config.imgUrl}${item.img}`}/>
                        </div>

                        })[9]
                    }
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2 ${Style.list}` }>
                    {manufacturerList.map((item)=>{ 
                        return <div className={`${Style.list} `}>    
                            <img className={`${Style.img}`} alt="manufacturere-logo" src={`${config.imgUrl}${item.img}`}/>
                        </div>

                        })[10]
                    }
                </div>
                <div className={`col-sm-12 col-lg-2 col-md-2 ${Style.list}` }>
                    {manufacturerList.map((item)=>{ 
                        return <div className={`${Style.list} `}>    
                            <img className={`${Style.img}`} alt="manufacturere-logo" src={`${config.imgUrl}${item.img}`}/>
                        </div>

                        })[11]
                    }
                </div>
            </div>
        </div>
    )

}

export default ManufacturerList