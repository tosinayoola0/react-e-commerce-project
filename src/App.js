import React from 'react';
import './App.css';
import { Route, Routes, BrowserRouter } from 'react-router-dom';
import AdminDashoard from './components/dashoard/admin-dashoard';
import UserDashoard from './components/dashoard/user-dashboard';
import ProductDetail from './components/detail';
import Index from './components/index/index'
import Signup from './components/signup/signup'
import Login from './components/login/login'
import Cart from './components/cart/cart';
import Profile from './components/profile';
import AccountOverView from './components/acct-overview';
import AccountManager from './components/account';
import ProfileDetail from './components/profile-detail';
import ChangePassword from './components/security/change-pswd';
import EditProfile from './components/profile-detail/edit';
import ConfirmOrder from './components/confirm-detail';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='' element={<Index/>} />
        <Route path='/signup' element={<Signup/>} />
        <Route path='/login' element={<Login/>} />
        <Route path ='/admin/:id/dashboard' element={<AdminDashoard/>} />
        <Route path ='/:id/dashboard' element={<UserDashoard/>} />
        <Route path='/product/:id/detail' element={<ProductDetail/>} />
        <Route path='user/:id/profile' element={<Profile/>}/>
        <Route path='/shopping-cart' element={<Cart/>}/>
        <Route path='/user/:id/account-detail' element={<AccountOverView/>}/>
        <Route path='/user/:id/profile-update' element={<AccountManager/>}/>
        <Route path='/user/:id/profile-detail' element={<ProfileDetail/>} />
        <Route path='/user/:id/change-password' element={<ChangePassword/>}/> 
        <Route path='/user/:id/edit-profile' element={<EditProfile/>} />
        <Route path='/confirm-order' element={<ConfirmOrder/>}/>
      </Routes>
      </BrowserRouter>
  );
}

export default App;
