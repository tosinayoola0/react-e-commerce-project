import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import profileService from "../services/profile/profileService";

export const createProfile = createAsyncThunk(
    'auth/create-profile',
    async(form, thunkAPI)=>{
        try {
            const response = await profileService.createProfile(form)
            return response.data
        }
        catch(err){
            const msg =(
                err.response || err.response.msg ||
                err.response.data
            ) || err.message || err.toString()
            return thunkAPI.rejectWithValue()
        }
    }
)

export const updateProfile = createAsyncThunk(
    'auth/update-profile',
    async({form, user}, thunkAPI)=>{
        try {
            const result = await profileService.updateProfile(form, user)
            return result
        } 
        catch(err){
            const msg = (
                err.response || err.response.msg || 
                err.response.data 
            ) || err.message || err.toString()
            return thunkAPI.rejectWithValue()
        }
    }
)



export const displayProfile = createAsyncThunk(
    'auth/display-profile',
    async(user, thunkAPI)=>{
        try{
            const res = await profileService.displayProfile(user.id)
            return res.data
        }
        catch(err){
            const msg = (
                err.response || err.response.msg ||
                err.response.data
            ) || err.message || err.toString()
            return thunkAPI.rejectWithValue()
        }
    }
)

let initialState = {
    profile: []
}

 const ProfileSlice = createSlice({
    name: 'profile',
    initialState,
    extraReducers: {
        
        [createProfile.fulfilled]: (state, action)=>{
            return Object.assign({}, state, {
                profile: action.payload
                //profile: state.profile.concat(action.payload)
                
            })
        },
        [createProfile.rejected]: (state, action)=>{
            return Object.assign({}, state, {
                profile: state.profile
            })
        },

        [updateProfile.fulfilled]: (state, action)=>{
            return Object.assign({}, state, {
                profile: action.payload
                //profile: state.profile.concat(action.payload)
                
            })
        },
        [updateProfile.rejected]: (state, action)=>{
            return Object.assign({}, state, {
                profile: state.profile
            })
        },
        [displayProfile.fulfilled]: (state, action)=>{
            return Object.assign({}, state, {
                profile: action.payload
            })
        },
        [displayProfile.rejected]: (state, action)=>{
            return Object.assign({}, state, {
                profile: state.profile
            })
        },     
    }
 })

 const {reducer} = ProfileSlice
 export default reducer