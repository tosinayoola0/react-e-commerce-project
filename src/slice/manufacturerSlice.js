import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { setMessage } from "./msg";
import dataService from "../services/data/dataService";



export const newManufacturer = createAsyncThunk(
    'manufacturer/add-manufacturer',
    async(form, thunkAPI)=>{
        try{
            const response = await dataService.createManufacturer(form)
            thunkAPI.dispatch(setMessage(response.data.msg))
            return response.data

        }
        catch(err){
            const msg = (err.message && err.response.msg &&
                err.response.data.msg
            ) || err.message || err.toString()
            thunkAPI.dispatch(setMessage(msg))
            return thunkAPI.rejectWithValue()
        }

    }

)

export const manufacturerLists = createAsyncThunk(
    'manufacturer/manufacturer-list',
    async(thunkAPI)=>{
        try{
            const response = await dataService.listManufacturer()
            //thunkAPI.dispatch(setMessage(response.data.msg))
            return response.data
        }
        catch(err){
            const msg = (
                err.response.message && err.message && 
                err.response.data.message
            ) || err.message || err.toString()
            thunkAPI.dispatch(setMessage(msg))
            return thunkAPI.rejectWithValue()
        }
    }
)



let initialState = {
    manufacturers: []
}


const manufacturerSlice = createSlice({
    name: 'manufacturer',
    initialState, 
    extraReducers: {
        [manufacturerLists.fulfilled]:(state, action)=>{
            return Object.assign({}, state, {
                manufacturers: state.manufacturers.concat(action.payload)
            })
        },
        [manufacturerLists.rejected]: (state, action)=>{
            return Object.assign({}, state, {
                manufacturers: state.manufacturers
            })
        },

        [newManufacturer.fulfilled]: (state, action)=>{
            return Object.assign({}, state, {
                manufacturer: state.manufacturers.concat(action.payload)
            })
        },
        [newManufacturer.rejected]: (state, action)=>{
            return Object.assign({}, state, {
                manufacturer: state.manufacturers
            })
        }

    }
})

const {reducer}= manufacturerSlice
export default reducer