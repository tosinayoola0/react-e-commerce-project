import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { setMessage } from "./msg";
import dataService from "../services/data/dataService";


export const newCategorySlice = createAsyncThunk(
    'category/new-category',
    async(form, thunkAPI)=>{
        try{
            const response = await dataService.createCategory(form)
            thunkAPI.dispatch(setMessage(response.data.msg))
            return response.data
        }
        catch(err){
            const msg = (err.data.msg && 
                err.response.data.msg &&
                err.response
            ) 
            ||  err.message || err.toSting()
            thunkAPI.dispatch(setMessage(msg))
            return thunkAPI.rejectWithValue()    
        }
    }
    
)

let initialState = {
    categories : []
}

const CategorySlice = createSlice({
    name: 'category',
    initialState,
    extraReducers:{
        [newCategorySlice.fulfilled]: (state, action)=>{
            return Object.assign({},state, {
                category : state.categories.concat(action.payload)
            })
        },
        [newCategorySlice.rejected]: (state, action)=>{
            return Object.assign({}, state, {
                category: state.categories
            })
        }
    }
})


const  {reducer} = CategorySlice
export default reducer