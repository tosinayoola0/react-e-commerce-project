import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import AuthService from "../services/auth/auth-service";


const initialState = {
    user: [],
}


export const register = createAsyncThunk(
    'auth/sign-up',
    async(formData, thunkAPI)=>{
        try{
            
            const response = await AuthService.userSignup(formData)
            console.log(response)
            return response.data
        }
        catch(err){
            const msg = (
                err.response && 
                err.response.data.message && 
                err.response.data
            ) ||
            err.message || err.toString()
            //thunkAPI.dispatch(setMessage(msg))
            return thunkAPI.rejectWithValue()
        }
    }
)


export const changePassword = createAsyncThunk(
    'auth/change-password',
    async(formData, thunkAPI)=>{
        try{
            console.log(formData)
            const response = await AuthService.changePassword( formData)
            return response.data
        }
        catch(err){
            const msg = (
                err.response || err. response.message ||
                err.response.data
            ) || err.message || err.toString()
            return thunkAPI.rejectWithValue
        }
    }
)

export const login = createAsyncThunk(
    'auth/login',
    async(formData, thunkAPI)=>{
        try{
            const response = await AuthService.userLogin(formData)
            return response.data
        }
        catch(err){
            const msg = (
                err.response || err.response.message ||
                err.response.data
            ) || err.message || err.toString()
            return thunkAPI.rejectWithValue()
        }
    }
)



export const AuthSlice = createSlice({
    name: 'auth',
    initialState, 
    extraReducers: {
        [register.fulfilled]:(state, action)=>{
            return Object.assign({}, state, {
                isLoggedIn: false,
                user: action?.payload
            })
        },
        [register.rejected]: (state, action)=>{
            return Object.assign({}, state, {
                newUser: state?.newNser
            })
        },

        [login.fulfilled]:(state, action) =>{
            return Object.assign({}, state, {
                isLoggedIn: true,
                user: action?.payload
            })
        },
        [login.rejected]: (state, action)=>{
            return Object.assign({},state, {
                isLoggedIn:false
            })
        },
        [changePassword.fulfilled]:(state, action)=>{
            return Object.assign({}, state, {
                newPassowrd: action?.payload
            })
        },
        [changePassword.rejected]: (state, action)=>{
            return Object.assign({},state,{
                newPassowrd: state.newPassowrd
            })
        }

    }
})


const {reducer} = AuthSlice
export default reducer