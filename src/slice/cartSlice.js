import { createSlice } from "@reduxjs/toolkit";
const initialState = {
    cart: [],
    totalPrice: ''
 }


const cartSlice = createSlice({
     name: 'cart',
    initialState,    
     reducers: {
        addToCart: (state, action)=>{
            let item = state.cart.find((item)=>item.id === action.payload.id)
            if(item){
                 state.cart.concat(item.quantity++)
            }else{
               return Object.assign({}, state, {
                cart: state.cart.concat(action.payload)
               }) 
            }
        },
        increaseQuantity: (state, action)=>{
            console.log(action)
            let item = state.cart.find((item)=>item.id=== action.payload.id )
                item.quantity++  
                item.total_price = item.quantity * item.unit_price

        },
        decreaseQuantity:(state, action)=>{
            console.log(action)
            let item = state.cart.find((item)=>item.id === action.payload.id)
            if(item.quantity >1){
                item.quantity--
                item.total_price = item.quantity * item.unit_price

            }
        },
        deleteItem: (state, action) =>{
            let delItem = state.cart.filter(item => item.id !== action.payload.id)
            return Object.assign({}, state, {
                cart: delItem
            })
        },
        calculateCost: (state, action) =>{
            let item = state.cart.find((item)=>item.id === action.payload.id)
            if(item){
                item.total_price = item.quantity * item.unit_price
            }
        }
     }



})


export const CartReducer = cartSlice.reducer
 export const {
    addToCart,
    deleteItem,
    increaseQuantity, 
    decreaseQuantity,
    calculateCost
 } = cartSlice.actions