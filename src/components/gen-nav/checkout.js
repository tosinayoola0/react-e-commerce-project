import React, { useState } from "react";
import { Link } from "react-router-dom";
import Style from './genNav.module.css'
import { ArrowBackIcon } from "../icons/icons";

const CheckOutNav = () =>{

    function goBack(){
        window.history.back()
    }


    return (
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <span class={`navbar-toggler ${Style.span}`} type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation" onClick={goBack}><ArrowBackIcon/></span>
                <Link class="navbar-brand" className={Style.link} href="#">Neat Store</Link>
                <h3 class="navbar-nav" className={Style.header}>Place Your Oder</h3>

                <div class="collapse navbar-collapse" id="navbarNav">

                </div>
            </div>
        </nav>
    )
}

export default CheckOutNav