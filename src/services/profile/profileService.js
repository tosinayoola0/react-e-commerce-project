import axios from "axios";
import AuthInterceptor from "../../interceptor/auth"
import { config } from "../../environment/env";



const createProfile = (data) =>{
    let url = `${config.baseUrl}/profile/create`
    return axios.post(url, data, {headers: AuthInterceptor()})
}

const displayProfile = (id) =>{
    let url = `${config.baseUrl}/profile/${id}/display`
    console.log(AuthInterceptor())
    return axios.get(url, {headers: AuthInterceptor()})
}

const updateProfile = (data, user) =>{
    let url = `${config.baseUrl}/profile/${user.uuid}/update`
    return axios.put(url, data, {headers: AuthInterceptor()})
}


const  ProfileService = {
    createProfile,
    updateProfile, 
    displayProfile
}
export default ProfileService