import React, {useState, useEffect} from "react";
import {AccountCircleIcon, SecurityIcon} from '../icons/icons'
import { useSelector } from 'react-redux'
import Style from './sidebarStyle.module.css'
import { Link } from 'react-router-dom'



const SettingsSidebar = () =>{
    const [show, setShow] = useState(false)
    const [options, setOption] = useState(false)
    const state = useSelector((data)=>data.authSlice.user)
    const [user, setUser] = useState({})


    function displayItems(){
        setShow(!show)
    }

    function displayOption(){
        setOption(!options)
    }
    useEffect(()=>{
        setUser(state)   
    })
    return (
        <div>
            <div  >
                <h5 className={Style.name}>Hello {user?.first_name} {user?.last_name}</h5>
                    
                    <div className={`${Style.header}`}>
                        <div className={Style.subMenu}>
                            
                            <p onClick={displayItems} className={Style.navTxt}>
                                <span className={Style.icon}>
                                    <AccountCircleIcon/>
                                </span>
                                Personal Detail
                            </p>
                            
                            {show ?
                                <div className={Style.txt}>
                                    <p><Link className={Style.link} to={`/user/${user.id}/profile-detail`}>Personal Details</Link></p>
                                </div>
                                : 
                                null
                            }
                        </div>
                        <div className={Style.subMenu}>
                            <p onClick={displayOption} className={Style.navTxt}>
                                <span className={Style.icon}>
                                    <SecurityIcon/>
                                </span>
                                Security Settings
                            </p>
                            {options ?
                                <div className={Style.txt}>
                                    <p><Link className={Style.link} to={`/user/${user.id}/change-password`}>Change Password</Link> </p>
                                    <p><Link className={Style.link} to={''}>Pin Setting</Link></p>

                                </div>
                                : 
                                null
                            }
                        </div>
                        
                    </div>
                </div>
        </div>
    )
}

export default SettingsSidebar