import AuthService from '../services/auth/auth-service'

 const AuthInterceptor = ()=>{
    const user = AuthService.getUser()
    if(user){
        return {Authorization: `Bearer ${user?.access_token}`}
    }{
        return {}
    }
}


export default AuthInterceptor