import React, {useEffect, useState} from 'react'
import SettingsSidebar from '../sidebar/optional-siderbar'
import Style from './style.module.css'
import { changePassword } from '../../slice/auth'
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux'

const ChangePassword = () =>{
    const [oldPassword, setOldPassword] = useState('')
    const [newpassword, setNewPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [hint, setHint] = useState('')
    const [pswdHint, setPswdHint] = useState('')
    const dispatch = useDispatch()
    const state = useSelector((data)=> data.authSlice.user)
    function checkPswd(){
        if(newpassword !== confirmPassword){
           return setHint('password, doesnt match, pls try again')
        }else{
            return setHint('')
        }
        
    }

    function pHint(){
        if(newpassword<7){
            setPswdHint('opps!!, passowrd cannot be less than 7 characters')
        }else{
            setPswdHint('')
        }
    }
    function onSubmit(e){
        e.preventDefault()
        const data = {
            old_password: oldPassword,
            new_password: newpassword,
            confirm_password: confirmPassword
        }
        dispatch(changePassword(data))
            .then((res)=>console.log(res))
    }
    return (
        <div className='container'>
            <div className={Style.dummie}></div>
            <div className='row'>
                <div className={`col-3 ${Style.sidebar}`}>
                    <SettingsSidebar/>
                </div>
                <div className='col-8'>
                    <div className={`${Style.form}`}>
                        <h3>Password Settings</h3>
                        <form onSubmit={onSubmit}>
                            <div className={`form-group ${Style.field}`}>
                                <input 
                                    onChange={((e)=>setOldPassword(e.target.value))} 
                                    name='oldPassword'
                                    placeholder='Enter your Old Password' 
                                    type='password'
                                    required
                                    className='form-control'
                                    onBlur={pHint}
                                />
                                <p className={Style.hint}>{pswdHint}</p>

                            </div>
                            <div className={`form-group ${Style.field}`}>
                                <input 
                                    onChange={((e)=>setNewPassword(e.target.value))} 
                                    name='newPassword'
                                    required
                                    placeholder='Enter Your new Password' 
                                    className='form-control'
                                    type='password'
                                    onBlur={pHint}


                                />
                                <p className={Style.hint}>{pswdHint}</p>

                            </div>
                            <div className={`form-group ${Style.field}`}>
                                <input 
                                    required={true}
                                    type='password'
                                    onChange={((e)=>setConfirmPassword(e.target.value))} 
                                    placeholder='Confirm Password' 
                                    name='confirmPassword' 
                                    className='form-control'
                                    onBlur={checkPswd}
                                />
                                <p className={Style.hint}>{hint}</p>

                            </div>
                            <div className={Style.field}>
                                <button className='btn btn-primary '>Change Password</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ChangePassword