import React,{useEffect, useState} from 'react';
import { useDispatch } from 'react-redux';
import { displayProductDetail, manufacturerDetail } from '../../slice/productSlice';
import { useParams } from 'react-router-dom';
import Style from './style.module.css'
import GenNavBar from '../gen-nav';
import Footer from '../footer';
import {config} from '../../environment/env'
import { HouseIcon, RemoveIcon, Divider, AddIcon } from '../icons/icons';
import {addToCart} from '../../slice/cartSlice'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const ProductDetail = () =>{
    const [productDetail, setProductDetail] = useState({})
    const [count, setCounter] = useState(1)
    const [otherProducts, setOtherProducts] = useState([])
    const dispatch = useDispatch()
    const params = useParams()
    const [hint, setHint] = useState('')
    
    useEffect(()=>{
        displayProductdetail()
        
    }, [])
    
    const increaseItem=(item)=>{
        if(count<item.stock){
            setCounter(count => count+1)    
            
        }
         if(count<item.stock){
            setHint(' ')

         }
         else{
            setHint('Product maximum stocked reached ')

         }
    }
    const decreaseItem=(item)=>{
       if(count>1){
            setHint(' ')
            setCounter(count=>count-1)

        }
    }

    
    function createCart(){
        let data = {
            unit_price: productDetail?.price,
            item_name: productDetail?.name,
            quantity: count,
            id: productDetail.id,
            img: productDetail?.img,
            total_price: count * productDetail.price
        }
        dispatch(addToCart(data))
        toast.success('Cart successfully updated' ,{autoClose: 2000, pauseOnHover: false })

    }
    
    function displayProductdetail (){
        let id = params.id
        dispatch(displayProductDetail(id))
        .unwrap()
        .then((res)=>{
            let response = res
            setProductDetail(response)
            displayOtherProducts(res.manufacturer_id)

        })
    }

    function displayOtherProducts(id){
        dispatch(manufacturerDetail(id))
            .then((res)=>{
                setOtherProducts(res?.payload.data)
            })
    }

    return (
        <div className={`${Style.container}`} >
            <GenNavBar/>
            <ToastContainer/>
            <div className={`${Style.body}`}>
                <div className={Style.category}>

                </div>
                <div className={`container ${Style.productDiv}`}>
                   
                    <div className={`row ${Style.row}` }>
                        <div className={`col-sm-11 col-lg-6 col-md-6 ${Style.imgDiv}`}>
                            <img
                                alt='product-img'
                                src={`${config.imgUrl}/${productDetail.img}`}
                                className={Style.prodImg}
                            />

                        </div>
                        <div className={`col-sm-11 col-lg-4 col-md-4 ${Style.detail}`}>
                            <div className={Style.title}>
                                <h2 className={Style.name}>{productDetail.name}</h2>
                                <p className={Style.brand}>Brand: {productDetail?.manufacturer}</p>
                            </div>
                            <Divider className={Style.divider}/>
                            <div className={Style.price}>
                                <p>N{productDetail.price}</p>
                            </div>
                            <Divider className={Style.divider}/>
                            <div className={Style.quantity}>
                                <div className={Style.select}>
                                    <span className={Style.decrease} onClick={()=>decreaseItem(productDetail)}><RemoveIcon/></span>
                                    <span className={Style.text}>{count}</span>
                                    <span onClick={()=>increaseItem(productDetail)} className={Style.decrease}> <AddIcon/> </span>
                                </div>     
                                <p className={Style.hint}>{hint}</p>
                                
                            </div>
                            <button className={`btn btn-success rounded ${Style.btn}`} onClick={createCart}>Add to Cart</button>
                            <Divider className={Style.divider}/>
                            <div className={Style.collect}>
                                <HouseIcon className={Style.collectIcon} />
                                <p>Pickup & pay on collection is available</p>
                            </div>
                            <Divider className={Style.divider}/>

                        </div>
                    </div>
                    <div className={`row ${Style.otherInfo}` }>
                       <p>Product Detail</p>
                        <Divider className={Style.divider}/>
                       
                       <div className={Style.description}>
                            <p>{productDetail.description}</p>
                        </div> 
                        <Divider className={Style.divider}/>

                        {(()=>{
                            if(productDetail?.memory ){
                                return (
                                    <div className={Style.specs}>
                                        <h4 className={Style.spectxt}>Specifications</h4>
                                        <Divider className={Style.divider}/>
                                        <div className={Style.keySpecs}>
                                            <p className={Style.keySpecsTitle}>KEY FEATURES</p>
                                            <div className={Style.keySpecsDiv}>
                                                <p>{productDetail?.display}</p>
                                                <p>{productDetail?.rear_camera}</p>
                                                <p>{productDetail?.front_camera}</p>
                                                <p> {productDetail?.memory}</p>
                                                <p>{productDetail?.platform}</p>
                                                <p>{productDetail?.processor}</p>
                                                <p>{productDetail?.network}</p>
                                            </div>

                                        </div>
                                        <div className={Style.keySpecs}>
                                            <p className={Style.keySpecsTitle}>SPECIFICATIONS</p>
                                            <div className={Style.keySpecsDiv}>
                                                <p><span className={Style.specName}>SKU:</span> {productDetail?.sku}</p>
                                                <p> <span className={Style.specName}>Product Line:</span> {productDetail?.line}</p>
                                                <p><span className={Style.specName}>Model:</span> {productDetail?.model}</p>
                                                <p><span className={Style.specName}>Size:</span> {productDetail?.size}</p>
                                                <p><span className={Style.specName}>Weight:</span> {productDetail?.weight}</p>
                                                <p><span className={Style.specName}>Color:</span> {productDetail?.colour}</p>
                                                <p><span className={Style.specName}>Bluetooth:</span> {productDetail?.bluetooth}</p>
                                                
                                            </div>

                                        </div>
                                    </div>
                                )
                            }   
                        })()}
                    </div>
                    {(()=>{
                        if(otherProducts[0]?.name ){
                            return(
                                <div className={`row ${Style.otherProducts}`}>
                                    <h3>More Items from this seller </h3>
                                    <div className={`col-sm-12 col-md-2 col-lg-2`}>
                                        {otherProducts.map((prooduct)=>{
                                            return<div key={prooduct?.id}>
                                                <div className={`${Style.otherProductsdiv}`}>
                                                    <img src={`${config.imgUrl}/${prooduct.img}`} className={`${Style.otherProductsImg}`}/>
                                                    <p className={Style.otherProductName}>{prooduct?.name} </p>
                                                    <p className={Style.otherProductPrice}>{prooduct?.price} </p>
                                                </div>
                                            </div>
                                        })[0]}
                                    </div>
                                    <div className={`col-sm-12 col-md-2 col-lg-2`}>
                                        {otherProducts.map((prooduct)=>{
                                            return<div key={prooduct?.id}>
                                                <div className={`${Style.otherProductsdiv}`}>
                                                    <img src={`${config.imgUrl}/${prooduct.img}`} className={`${Style.otherProductsImg}`}/>
                                                    <p className={Style.otherProductName}>{prooduct?.name} </p>
                                                    <p className={Style.otherProductPrice}>{prooduct?.price} </p>

                                                </div>
                                            </div>
                                        })[1]}
                                    </div>
                                    <div className={`col-sm-12 col-md-2 col-lg-2`}>
                                        {otherProducts.map((prooduct)=>{
                                            return<div key={prooduct?.key}>
                                                <div className={`${Style.otherProductsdiv}`}>
                                                    <img src={`${config.imgUrl}/${prooduct.img}`} className={`${Style.otherProductsImg}`}/>
                                                    <p className={Style.otherProductName}>{prooduct?.name} </p>
                                                    <p className={Style.otherProductPrice}>{prooduct?.price} </p>
                                                </div>
                                            </div>
                                        })[2]}
                                    </div>
                                    <div className={`col-sm-12 col-md-2 col-lg-2`}>
                                        {otherProducts.map((prooduct)=>{
                                            return<div key={prooduct?.key}>
                                                <div className={`${Style.otherProductsdiv}`}>
                                                    <img src={`${config.imgUrl}/${prooduct.img}`} className={`${Style.otherProductsImg}`}/>
                                                    <p className={Style.otherProductName}>{prooduct?.name} </p>
                                                    <p className={Style.otherProductPrice}>{prooduct?.price} </p>
                                                </div>
                                            </div>
                                        })[3]}
                                    </div>
                                    <div className={`col-sm-12 col-md-2 col-lg-2`}>
                                        {otherProducts.map((prooduct)=>{
                                            return<div key={prooduct?.key}>
                                                <div className={`${Style.otherProductsdiv}`}>
                                                    <img src={`${config.imgUrl}/${prooduct.img}`} className={`${Style.otherProductsImg}`}/>
                                                    <p className={Style.otherProductName}>{prooduct?.name} </p>
                                                    <p className={Style.otherProductPrice}>{prooduct?.price} </p>
                                                </div>
                                            </div>
                                        })[4]}
                                    </div>
                                    <div className={`col-sm-12 col-md-2 col-lg-2`}>
                                        {otherProducts.map((prooduct)=>{
                                            return<div key={prooduct?.key}>
                                                <div className={`${Style.otherProductsdiv}`}>
                                                    <img src={`${config.imgUrl}/${prooduct.img}`} className={`${Style.otherProductsImg}`}/>
                                                    <p className={Style.otherProductName}>{prooduct?.name} </p>
                                                    <p className={Style.otherProductPrice}>{prooduct?.price} </p>
                                                </div>
                                            </div>
                                        })[5]}
                                    </div>
                                </div>
                            )
                        }
                    })()}
                </div>
            </div>
            <div className={`${Style.footer}`}>
            <Footer  />

            </div>
        </div>
    )
}
export default ProductDetail