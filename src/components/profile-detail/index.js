import React, {useEffect, useState} from 'react'
import { useSelector } from 'react-redux'
import Style from './style.module.css'
import { Link } from 'react-router-dom'
import SettingsSidebar from '../sidebar/optional-siderbar'
function ProfileDetail(){
    const [user, setUser] = useState({})
    const [profile, setProfile] = useState({})
   
    const state = useSelector((data)=>data)
    useEffect(()=>{
        setUser(state.authSlice.user)
        setProfile(state.profileSlice.profile.data)
    })
    return(
        <div className='container'> 
            <div className={Style.dummie}></div>
            <div className={`row ${Style.row} `}>
                <div className='col-3'>
                    <SettingsSidebar/>
                </div>                
                <div className='col-8'>
                    <div className={Style.detail}>
                        <div className={Style.detailHeader}>
                            <p>Profile Detail</p>
                            <Link to={`/user/${user.id}/profile`} className={Style.edit}><p >Edit Profile</p></Link>                            
                        </div>
                        <div className={Style.detailDiv}>
                            <p className={Style.hint}>First Name</p>
                            <p>{profile?.first_name}</p>
                        </div>
                        <hr/>
                        
                        <div className={Style.detailDiv}>
                            <p className={Style.hint}>Middle Name</p>
                            <p>{profile?.other_name}</p>
                        </div>
                        <hr/>

                        <div className={Style.detailDiv}>
                            <p className={Style.hint}>Last Name</p>
                            <p>{profile?.last_name}</p>
                        </div>
                        <hr/>

                        <div className={Style.detailDiv}>
                            <p className={Style.hint}>email</p>
                            <p>{user.email}</p>
                        </div>
                        <hr/>

                        <div className={Style.detailDiv}>
                            <p className={Style.hint}>Gender</p>
                            <p>{profile?.gender}</p>
                        </div>
                        <hr/>

                        <div className={Style.Style}>
                            <p className={Style.hint}>Phone Number</p>
                            <p>{profile?.phone_number}</p>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    )
}


export default ProfileDetail