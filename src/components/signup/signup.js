import React, {useState, useEffect} from "react";
import * as Yup from 'yup'
import { Field, Form , Formik, useFormik} from "formik"
import regStyle from './signup.module.css'
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { register } from "../../slice/auth";
import { useNavigate } from "react-router-dom";

const validate = values=>{
    const errors = {}
    if(!values.username){
        errors.username = 'username Required'
    }else if(values.username.length< 3){
        errors.username = 'username must b greater than 3 characters '
    }
    if(!values.email){
        errors.email = 'Email is Required'
    }
    else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)){
        errors.email = 'invalid email'
    }

    if(!values.password){
        errors.password = 'password is required'

    }
    else if(values.password.length < 5){
        errors.password = 'password field cannot be less than 5'
    }
    return errors
}


const Signup =() =>{
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
            username: ''
        },validate,
        onSubmit: values =>{
            dispatch(register(values))
                .unwrap()
                .then((res)=>{
                    console.log(res)
                    navigate('/login')
                    
                }
                )
            console.log(values)
        }
        
    }) 
   

    
    return (
        <div className={` ${regStyle.body}`}>
            <div className={`${regStyle.form} row` } >
               <h3 className={`${regStyle.title}`}>Create An Account</h3>
                <form  onSubmit={formik.handleSubmit}>
                    
                    <div className={`form-group col-8 ${regStyle.fields}`}>
                        <input  
                            name="username"  
                            placeholder="pick your username"  
                            value={formik.values.username}
                            onChange={formik.handleChange}
                            className={`form-control `}

                        />
                        {formik.errors.username  ?(<span>{formik.errors.username}</span>): null}
                    </div>

                    <div className={`form-group col-8 ${regStyle.fields}`}>
                        <input 
                            name="email" 
                            type="email" 
                            value={formik.values.email}
                            placeholder="Enter your email address" 
                            className="form-control"
                            onChange={formik.handleChange}
                        />
                                
                        {formik.errors.email  ? (<span>{formik.errors.email}</span>): null}
                    </div>

                    <div className={`form-group col-8 ${regStyle.fields}`}>
                        <input 
                            placeholder="enter Your Password" 
                            name="password" 
                            className="form-control"
                            type="password"
                            value={formik.values.password}
                            onChange={formik.handleChange}
                                
                        />
                        {formik.errors.password ? (<span>{formik.errors.password}</span>): null}
                    </div>
                            
                    <button type="submit" className="btn btn-success"> Register</button>
                </form>
                <div className={regStyle.already}><p>Already have an acount, <Link to='/login'>login</Link></p></div>
            </div>

        </div>
    )
}

export default Signup