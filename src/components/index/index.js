import React from "react";
import IndexStyle from './index.module.css'
import ProductList from "../list";
import GenNavBar from "../gen-nav";
import Imgs from "../imgs/img";
import ImageSlider from "../imgs/imgSlider";
import ManufacturerList from "../manufacturer/manufacturer-list";
import Footer from '../footer';

const Index =() =>{
    return (
        <div>
            <div className={`${IndexStyle.nav}`}>
                <GenNavBar/>
            </div>
            <div className="container"> 
                <div className={`${IndexStyle.slide} row`}>
                    <div className={`${IndexStyle.slider} col-12`}>
                        <ImageSlider imgs={Imgs}/>
                    </div>
                </div>
                <div className="row"></div>
                <div className={`row ${IndexStyle.list}`}>
                    <ProductList/>

                </div>
                <div className="row">
                    <ManufacturerList/>
                </div>
                
            </div>
            <div className="row">
                    <Footer/>
                </div>
        </div>
    )
}

export default Index