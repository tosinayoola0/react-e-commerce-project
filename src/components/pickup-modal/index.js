import React, {useState, useEffect} from "react";
import { Modal } from "react-bootstrap";
import Style from './style.module.css'
import { Locations } from "./constants";
import {yupResolver} from '@hookform/resolvers/yup'
import { CalendarMonthIcon } from "../icons/icons";
import * as Yup from 'yup'
import { useForm } from 'react-hook-form';
import { createAddress } from "../../slice/addressSlice";
import { useDispatch } from "react-redux";


const PickupModal = ({showModal, setShowModal})=>{
    const [states, setStates] = useState([])
    const [cities, setCities] = useState([])
    const [addresses, setAddress] = useState([])
    const [selectedState, setStateVal] = useState('')
    const [selectedLga, setLgaVal] = useState('')
    const [selectedAddress, setAddresVal] = useState('')
    const dispatch =useDispatch() 

    
    useEffect(()=>{
        getStates()
        getLgas(states[0]?.state.name)
    }, [])
    
    

    
    function stateChange(state){
        setStateVal(state)
        getLgas(state)
    }

    function lgaChange(lga){
        setLgaVal(lga)
        getAddress(lga)
    }
    function close(){
        setShowModal(!showModal)
    }   

    const validationSchema = Yup.object().shape({
        address: Yup.string()
            .required('Address is needed'),
        state: Yup.string()
            .required('state is required'),
        lga: Yup.string()
            .required('LGA is required')
    })

    const {register, handleSubmit, reset, setValue, getValues,   errors, formState} = useForm({
        resolver: yupResolver(validationSchema)
     }) 
    function  getStates(){
        let s = []
        Locations.forEach((x) => {
            s.push(x)
            setStates(s)
        });
    }      


    function getLgas(state){
        states.forEach(x=>{
            if(state==x.state.name){
                setCities(x.state.cities)
            }
        })
    }

    function getAddress(city){
        console.log(city)
        states.forEach(x=>{
            x?.state?.cities.forEach((y)=>{
                if(city=== y.name){
                    setAddress(y.location)
                }
            })
        })
    }
    function submit(){
        const data = {
            state: selectedState,
            address: selectedAddress,
            lga: selectedLga
        }
        dispatch(createAddress(data))
        console.log(data)
        close()
    }
    
    
    
    return (
        <>
            <Modal contentClassName={Style.body} show={showModal}>
                <Modal.Title><h3 className={Style.title}>Select Your Pickup Location</h3></Modal.Title>
                <Modal.Body className="container">
                    <div className={`row ${Style.formcontent}`}>
                        <div className={`col-sm-12 col-lg-6 col-md-6 ${Style.select}`}>
                            <div className="form-group">
                                <select
                                    {...register('state', {
                                        required: "Required"
                                    })}
                                    className={'form-control'}             
                                    onChange={e=>stateChange(e.target.value)}
                                >
                                    {states?.map((state)=>(
                                        <option key={state.state.id}>
                                            {state.state.name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        </div>
                        <div className={`col-sm-12 col-lg-6 col-md-6 ${Style.select}`}>
                            <div className="form-group">
                                <select
                                    {...register('cities', {
                                        required: "Required"
                                    })}
                                    className={'form-control'}
                                    onChange={e=>lgaChange(e.target.value)}
                                >
                                    {cities?.map((city)=>(
                                        <option key={city.id}>
                                            {city.name}
                                            
                                        </option>
                                    ))}
                                </select>
                            </div>
                        </div>
                        <div className={'col-12'}>
                            {addresses?.map((address)=>(
                                <div id={address.id} className={Style.addDiv}>
                                    <div className={Style.input}>
                                        <input
                                            type="radio"
                                            onChange={e=>setAddresVal(address)}
                                            key={address?.id}
                                        />
                                    </div>
                                    <div className={Style.address}>
                                        <h4>{address.name}</h4>
                                        <p>{address.address}</p>
                                    </div>
                                    <div className={Style.time}>
                                        <p className={Style.hour}><CalendarMonthIcon/><span>Opening Hour</span></p>
                                        <p>{address.opening_time}</p>
                                    </div>

                                </div>
                            ))}
                        </div>
                    </div>

                    <div className={Style.btns}>
                        <button className="btn btn-danger" onClick={close}>close</button>
                        <button className="btn btn-success" onClick={submit}>Submit</button>
                        
                    </div>
                </Modal.Body>
            </Modal>
        </>
    )
}

export default PickupModal